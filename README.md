# Hanabot
The best utility bot, primarily for NSFW Discord servers.

The library for interacting with the API, or the bot's features, work for a general public bot. Feel free to self-host or make your own public instance.

Profile pic and name chosen by my friend Sophie, who also pays for bot hosting <3

[You can view more documentation here.](https://gitlab.com/hana.flower/hanabot/-/wikis/home)

## How to run
[Check here!](https://gitlab.com/hana.flower/hanabot/-/wikis/Self-Hosting)

### What you need to change
* You will need to manually add onboarding information to the database using something like Project Fauxton. `/onboarding` is a WIP setup wizard.
* You also need to change the `ping_server` function in `database.py` to get a document with an ID that you know, or some other lightweight operation. Or you can disable that entirely in `main.py` by commenting the line starting with `ping_task = `

You should run `/settings` to configure the bot when you first add it to the server.

## Features
[Check here!](https://gitlab.com/hana.flower/hanabot/-/wikis/About)

## Terms of service
Feel free to use any of this, but check [LICENSE](LICENSE) I guess.

Put a link to the bot's source code in the bot's description if you use it.

For privacy info, see [here](https://gitlab.com/hana.flower/hanabot/-/wikis/Privacy).

*Originally made by 花 <3#0676 (hana) with love and passion <3*