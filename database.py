# i still don't think this is how you use couchdb but whatever
import couchdb
import asyncio
import const
from time import time
from math import floor
import cProfile
import update_db_docs

# connect to the CouchDB server
server = couchdb.Server(const.couchdb_server)
server.resource.credentials = (const.couchdb_username, const.couchdb_password)

# get any document
def get_doc(database, mango_query, retry_count=0, max_retries=5):
    print("Getting doc")
    print(mango_query)
    db = server[database]
    try:
        results = list(db.find(mango_query))

        if results:
            for doc in results:
                version = doc.get('version', None) # every document SHOULD have a version number so no error checking here. if it doesn't then you probably screwed up tbh (or you're getting a claim from a REALLY old version)
                db_version = getattr(const, f"db_{database}_version") # this checks the version number the document should be at
                if version < db_version:
                    print(f"Need to update {database}")
                    # logic for updating the doc here
                    update_function = getattr(update_db_docs, f'update_{database}_version')
                    updated_doc = update_function(doc)
                    # Retry by calling the same function with an incremented retry_count
                    if retry_count < max_retries:
                        return get_doc(database, mango_query, retry_count=retry_count + 1)
                    else:
                        print("Max retries reached")
                        return "Max retries reached"
            return results
        else:
            print("No documents found")
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        print("No documents found")
        return "No documents found" # to-do: make this return an empty list instead and change the checks to account for that

# update any document
def update_doc(database, mango_query, update_dict, retry_count=0, max_retries=5):
    db = server[database]
    # force the query limit to 1
    mango_query["limit"] = 1

    try:
        results = list(db.find(mango_query))

        if results:
            doc = results[0] # we only want to update one document at a time

            # we still need to update the doc version before updating its values
            # so this block of code is for updating the document version
            version = doc.get('version', None) # every document SHOULD have a version number so no error checking here. if it doesn't then you probably screwed up tbh (or you're getting a claim from a REALLY old version)
            db_version = getattr(const, f"db_{database}_version") # this checks the version number the document should be at
            if version < db_version: 
                print(f"Need to update {database}")
                # logic for updating the doc here
                update_function = getattr(update_db_docs, f'update_{database}_version')
                update_function(doc)
                # Retry by calling the same function with an incremented retry_count
                if retry_count < max_retries:
                    return update_doc(database, mango_query, update_dict, retry_count=retry_count + 1)
                else:
                    print("Max retries reached")
                    return "Max retries reached"

            # update the doc values
            for key, value in update_dict.items():
                if key in doc:
                    doc[key] = value
                else:
                    print(f"Key '{key}' not found in the document")
            db[doc["_id"]] = doc  # Save the updated document
            return doc # return the updated dictionary. NOT a list
        else:
            print("No documents found")
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        print("No documents found")
        return "No documents found"

# delete any document
def delete_doc(database, mango_query):
    db = server["claiming"]
    try:
        results = db.find(mango_query)
        deleted_count = 0
        
        for result in results:
            db.delete(result)
            deleted_count += 1
        
        if deleted_count > 0:
            return
        else:
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        return "No documents found"

# insert claim
def insert_claim(claimed, master, guild, interaction_id, note, message_id="0"):
    db = server["claiming"]
    values = {
        "version": const.db_claiming_version,
        "id": interaction_id,
        "users": {
            "master": master,
            "claimed": claimed
        },
        "guild_id": guild,
        "message_id": message_id,
        "timestamp": str(floor(time())),
        "note": note
    }
    db.save(values)

# get claim
# this is deprecated
def get_claim(guild_id, claimed_id, master_id):
    print(f"Getting claim. guild_id: {guild_id}, claimed_id: {claimed_id}, master_id: {master_id}")
    db = server["claiming"]
    try:
        mango_query = {
            "selector": {
                "claimed_id": str(claimed_id),
                "master_id": str(master_id),
                "guild_id": guild_id
            },
            "limit": 1
        }

        claim_objects = list(db.find(mango_query))

        if claim_objects:
            claim_object = claim_objects[0]
        else:
            print("Claim does not exist")
            return "Error: Claim does not exist"
    except couchdb.http.ResourceNotFound:
        print("Claim does not exist")
        return "Error: Claim does not exist"
    return claim_object

# delete claim
def delete_claim(message_id):
    db = server["claiming"]
    try:
        mango_query = {
            "selector": {
                "message_id": message_id
            },
            "limit": 1
        }

        results = db.find(mango_query)
        deleted_count = 0
        
        for result in results:
            db.delete(result)
            deleted_count += 1
        
        if deleted_count > 0:
            print(f"{deleted_count} documents with message_id {message_id} deleted.")
        else:
            print("Claim does not exist")
            return "Error: Claim does not exist"
    except couchdb.http.ResourceNotFound:
        print(f"Claim with message_id {message_id} not found.")

# get member
def get_member(member, guild_id):
    print(f"Getting member. member: {member}, guild_id: {guild_id}")
    db = server["members"]
    try:
        mango_query = {
            "selector": {
                "user_id": str(member),
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        member_objects = list(db.find(mango_query))

        if member_objects:
            member_object = member_objects[0]
            version = member_object.get('version', None)  # Get the "version" key from the member_object
            if version < const.db_members_version:
                print("Need to update member")
                # logic for updating the member here
                update_db_docs.update_members_version(member_object)
                return "Retry"
        else:
            print("Member not registered")
            init_member(member, guild_id)
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Not found")
        init_member(member, guild_id)
        return "Retry"
    return member_object

# update member
def update_member(member, guild_id, update_dict):
    db = server["members"]
    try:
        mango_query = {
            "selector": {
                "user_id": str(member),
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        member_objects = list(db.find(mango_query))

        if member_objects:
            member_object = member_objects[0]
            version = member_object.get('version', None)  # Get the "version" key from the member_object
            if version < const.db_members_version:
                # logic for updating the member here
                update_db_docs.update_members_version(member_object)
                return update_member(member, guild_id, update_dict)
            for key, value in update_dict.items():
                if key in member_object:
                    member_object[key] = value
                else:
                    print(f"Key '{key}' not found in the document")
            db[member_object["_id"]] = member_object  # Save the updated document
        else:
            print("Member not registered")
            init_member(master, guild_id)
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Member not registered")
        init_member(master, guild_id)
        return "Retry"
    return member_object

# init member
def init_member(user_id, guild_id):
    db = server["members"]
    values = {
        "user_id": str(user_id),
        "version": const.db_members_version,
        "guild_id": str(guild_id),
        "claims": {
            "count": 0,
            "claimed_ids": []
        },
        "warns": {
            "count": 0,
            "list": []
        }
    }
    db.save(values)

# add claim to waitlist
def add_claim_to_waitlist(claimed, master, guild_id, note):
    db = server["claims_waitlist"]
    values = {
        "claimed_id": str(claimed),
        "master_id": str(master),
        #"message_id": str(message_id),
        "version": const.db_claims_waitlist_version,
        "guild_id": str(guild_id),
        "note": note,
        "accepted": []
    }
    db.save(values)

# get waitlist claim
def get_waitlist_claim(guild_id, claimed=None, master=None):
    print(f"Getting waitlist claim. guild_id: {guild_id}, claimed: {claimed}, master: {master}")
    db = server["claims_waitlist"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        if claimed is not None:
            mango_query["selector"]["claimed_id"] = str(claimed)

        if master is not None:
            mango_query["selector"]["master_id"] = str(master)

        claim_objects = list(db.find(mango_query))

        if claim_objects:
            claim_object = claim_objects[0]
        else:
            print("Waitlist claim not registered")
            return "Waitlist claim not registered"
    except couchdb.http.ResourceNotFound:
        print("Waitlist claim not registered")
        return "Waitlist claim not registered"
    return claim_object

# update waitlist claim
def update_waitlist_claim(claimed, master, guild_id, update_dict):
    db = server["claims_waitlist"]
    try:
        mango_query = {
            "selector": {
                "claimed_id": str(claimed),
                "master_id": str(master),
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        claim_objects = list(db.find(mango_query))

        if claim_objects:
            claim_object = claim_objects[0]
            for key, value in update_dict.items():
                if key in claim_object:
                    claim_object[key] = value
                else:
                    print(f"Key '{key}' not found in the document")
            db[claim_object["_id"]] = claim_object  # Save the updated document
        else:
            print("Waitlist claim not registered")
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Waitlist claim not registered")
        return "Retry"
    return claim_object

# delete waitlist claim
def delete_waitlist_claim(claimed, master, guild_id):
    db = server["claims_waitlist"]
    try:
        mango_query = {
            "selector": {
                "claimed_id": str(claimed),
                "master_id": str(master),
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))
        deleted_count = 0
        
        for result in results:
            db.delete(result)
            deleted_count += 1
        
        if deleted_count > 0:
            print("Waitlist claim deleted.")
            #print(f"{deleted_count} documents with message_id {message_id} deleted.")
        else:
            print("Claim does not exist")
            return "Error: Claim does not exist"
    except couchdb.http.ResourceNotFound:
        print("Claim does not exist")

# initialise guild
def init_guild(guild_id):
    db = server["guilds"]
    values = {
        "guild_id": str(guild_id),
        "version": "6",
        "important_channel_ids": {
            "claiming": "0",
            "claim_list": "0",
            "ban_list": "0",
            "welcome": "0",
            "warn_list": "0",
            "member_age_broadcast": "0"
        },
        'important_role_ids': {
            'claimed': "0",
            "master": "0",
            "member": "0"
        },
        "messages": {
            "welcome": {}
        },
        "features": { # disable all by default
            "claiming": False,
            "accountagecheck": False,
            "welcoming": False,
            "banlist": False,
            "memberagecheck": False,
            "onboarding": False
        },
        "min_acc_age": 24,
        "reinvite_url": "",
        "warn_limit": 3,
        "claim_limit": 3,
        "rules_buttons_swapped": False,
        "rules_message_link": ""
    }
    db.save(values)

# get guild
def get_guild(guild_id):
    print(f"Getting guild. guild_id: {guild_id}")
    db = server["guilds"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))
        
        # Check if any documents were found
        if results:
            guild_object = results[0]
            version = guild_object.get('version', None)  # Get the "version" key from the claim_object
            if version < const.db_guilds_version:
                print("The guild needs to be updated")
                update_db_docs.update_guilds_version(guild_object)
                return "Retry"
        else:
            print(f"The guild {guild_id} is not registered, time to do that")
            init_guild(guild_id)
            return "Retry"

        return guild_object

    except couchdb.http.ResourceNotFound:
        print(f"The guild {guild_id} is not registered, time to do that")
        init_guild(guild_id)
        return "Retry"

# update guild
def update_guild(guild_id, update_dict):
    db = server["guilds"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))

        if results:
            guild_object = results[0]
            version = guild_object.get('version', None)  # Get the "version" key from the claim_object
            if version < const.db_guilds_version:
                # logic for updating the guild version here
                update_db_docs.update_guilds_version(guild_object)
                return update_guild(guild_id, update_dict)

            # Update specific keys in the document
            for key, value in update_dict.items():
                nested_keys = key.split('.')
                current_dict = guild_object
                for nested_key in nested_keys[:-1]:
                    if nested_key in current_dict:
                        current_dict = current_dict[nested_key]
                    else:
                        print(f"Key '{nested_key}' not found in the document")
                        return "Retry"
                current_dict[nested_keys[-1]] = value

            db[guild_object["_id"]] = guild_object  # Save the updated document
        else:
            print("Guild not registered")
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Guild not registered")
        return "Retry"
    return guild_object


# init onboarding
def init_onboarding(guild_id):
    db = server["makeshift_onboarding"]
    values = {
        "guild_id": str(guild_id),
        "version": const.db_makeshift_onboarding_version,
        "questions": {}
    }
    db.save(values)

# get onboarding
def get_onboarding(guild_id):
    print(f"Getting guild onboarding. guild_id: {guild_id}")
    db = server["makeshift_onboarding"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))
        
        # Check if any documents were found
        if results:
            onboarding_object = results[0]
            version = onboarding_object.get('version', None)  # Get the "version" key from the claim_object
            if version < const.db_makeshift_onboarding_version:
                print("The guild needs to be updated")
                update_db_docs.update_guilds_version(guild_object)
                return "Retry"
        else:
            #print(f"The guild {guild_id} is not registered, time to do that")
            return "Guild onboarding not found"

        return onboarding_object

    except couchdb.http.ResourceNotFound:
        #print(f"The guild {guild_id} is not registered, time to do that")
        return "Guild onboarding not found"

# update onboarding
def update_onboarding(guild_id, update_dict):
    db = server["makeshift_onboarding"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))

        if results:
            onboarding_object = results[0]
            version = onboarding_object.get('version', None)  # Get the "version" key from the claim_object
            if version < const.db_makeshift_onboarding_version:
                print("The guild needs to be updated")
                update_db_docs.update_guilds_version(guild_object)
                return "Retry"
            for key, value in update_dict.items():
                if key in onboarding_object:
                    onboarding_object[key] = value
                else:
                    print(f"Key '{key}' not found in the document")
            db[onboarding_object["_id"]] = onboarding_object  # Save the updated document
        else:
            print("Guild onboarding not registered")
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Guild onboarding not registered")
        return "Retry"
    return onboarding_object


# keeping the connection alive
# this may not be necessary. only enable this if queries can sometimes be much slower than they should be.
async def ping_server():
    while True:
        print("Pinging the couchdb server...")
        # some random lightweight operation
        # it gets the document for The Lab
        # to-do: change this because leaking couchDB IDs is a terrible fucking idea lmfao but this is the document for my test server so who cares
        # although I do want to do it with the library
        db = server["guilds"]
        doc = db.get("345b19e3f425ec7fc7d51761be00fdd0")
        print("Pinged the couchdb server.")
        await asyncio.sleep(60)


