# defining
import asyncio
import websockets
import random
import json
import time
import requests
import cProfile
import sys

# custom libraries
import handling
import http_api
import database
import tools

interrupt_event = asyncio.Event()
stop_heartbeat_event = asyncio.Event()


# maybe these globals are not necessary but i am too scared to try removing them
global last_sequence_number
last_sequence_number = None
global seq
seq = None
global session_id
global resume_gateway_url_to_use
resume_gateway_url_to_use = ""
global identified
identified = False
global GATEWAY_INFO_TO_USE

global websocket_connections
websocket_connections = {"connections": {}}

import const
TOKEN = const.TOKEN

# when receiving message
async def on_message(shard, message):
    # get websocket configuration
    this_websocket_connection = websocket_connections["connections"][shard]

    websocket = this_websocket_connection["websocket"]
    last_sequence_number = this_websocket_connection["last_sequence_number"]
    seq = this_websocket_connection["seq"]
    resume_gateway_url = this_websocket_connection["resume_gateway_url"]

    # log message
    print(f"Received message on shard {shard}: {message}")
    print(f"last_sequence_number: {last_sequence_number}, seq: {seq}")

    # get opcode
    data = json.loads(message)
    op_code = data["op"]

    # sequence numbers
    if "s" in data and op_code not in (11, 10, 9, 7, 1): # these have null for the s number
        this_websocket_connection["last_sequence_number"] = data["s"]

    # handling opcodes
    
    # 1: Heartbeat
    # to-do: fix this (even though it is extremely rare and this literally never fucking happened to me LMAO)
    if op_code == 1:
        print("Heartbeat requested by server")
        http_api.send_message(1137870797160071201, {"content": f"<@1008711808544149524> OP 1 received. last_sequence_number: {last_sequence_number}"})
        # tell the task to immediately send the heartbeat
        '''
        interrupt_event.set()
        interrupt_event.clear()
        '''

    # 7: Reconnect
    elif op_code == 7:
        print("Reconnect requested")
        #http_api.send_message(1137870797160071201, {"content": f"<@1008711808544149524> OP 7 received. last_sequence_number: {last_sequence_number}"})
        #http_api.send_message("1137870797160071199", {"content": f"<@1008711808544149524> Session id: {session_id}"})
        to_send_resume = True
        this_websocket_connection["seq"] = last_sequence_number

        # the goal is to get this returning to actually resume, but for some reason it causes invalid session no matter what
        # the data is correct but the session gets invalidated for no fucking reason
        #return ("Resume", shard)

        # this infinite recursion is terrible but for some stupid reason it's the only way that things work
        # thanks discord for not EXPLAINING WHY THE SESSION WAS FUCKING INVALIDATED
        await run_websocket(shard, this_websocket_connection["gateway_info"], "Resume")

    # 9: Invalid Session
    elif op_code == 9:
        print("Session invalidated")
        http_api.send_message(1137870797160071201, {"content": f"<@1008711808544149524> OP 9 received. last_sequence_number: {last_sequence_number}"})
        if data["d"] == False:
            to_send_resume = False
        else:
            to_send_resume = True
        #await run_websocket(shard, this_websocket_connection["gateway_info"], "Resume")

    # 10: Hello
    elif op_code == 10:
        heartbeat_interval = data["d"]["heartbeat_interval"]
        this_websocket_connection["heartbeat_interval"] = heartbeat_interval
        print(f"Received Hello packet with heartbeat interval: {heartbeat_interval}")

        # start heartbeat task
        asyncio.create_task(send_heartbeat(websocket, heartbeat_interval))

        if resume_gateway_url != "":
            print(f"last_sequence_number: {last_sequence_number}, seq: {seq}")
            await send_resume(shard)
        else:
            #print("Not identified yet, sending Identify...")
            await send_identify(shard)

    # 11: Heartbeat ACK
    #elif op_code == 11:
        #print("Heartbeat ACK received")
        #await handle_heartbeat_ack(websocket)

    # 0: Event
    elif op_code == 0:
        print("Event received")
        event_name = data["t"]

        # handle events
        # ready
        if event_name == "READY":
            session_id = data["d"]["session_id"]
            this_websocket_connection["session_id"] = session_id

            resume_gateway_url = data["d"]["resume_gateway_url"]
            print(resume_gateway_url)
            resume_gateway_url_to_use = f"{resume_gateway_url}/?v=9&encoding=json"
            this_websocket_connection["resume_gateway_url"] = resume_gateway_url_to_use
            print(resume_gateway_url_to_use)

            print(f"Logged in as user: \"{data['d']['user']['username']}#{data['d']['user']['discriminator']}\" (UID: {data['d']['user']['id']})")
            print(f"This connection is using shard {data['d']['shard']}")

        # interactions
        if event_name == "INTERACTION_CREATE":
            await handling.handle_interaction(data)

        # other events
        elif event_name == "MESSAGE_CREATE":
            await handling.handle_message_create(data)
        
        elif event_name == "GUILD_BAN_ADD":
            handling.ban_list_automate(data)

        elif event_name == "GUILD_MEMBER_ADD":
            status = handling.kick_new_accounts(data)

            if status != "Account kicked":
                # welcome messages
                guild_info = database.get_guild(data["d"]["guild_id"])
                if guild_info["features"]["welcoming"] == True:
                    message_data_unformatted = guild_info["messages"]["welcome"]
                    if message_data_unformatted == "{}": return # no welcome message sent
                    message_data = tools.format_message(message_data_unformatted, "{user_id}", data["d"]["user"]["id"])
                    http_api.send_message(guild_info["important_channel_ids"]["welcome"], message_data)

    return
            

# handle opcodes

# sending heartbeats
async def send_heartbeat(websocket, heartbeat_interval):
    jitter = random.random()
    print("Heartbeat task started")
    while not stop_heartbeat_event.is_set():
        heartbeat_data = {
            "op": 1,
            "d": last_sequence_number,
        }
        print("Sending heartbeat after " + str(heartbeat_interval * jitter))

        await asyncio.sleep(heartbeat_interval * jitter / 1000)

        # DO NOT UNCOMMENT THIS CODE IT WILL BREAK THE BOT
        # op 1 from server never happens
        '''
        # Wait for either the interval op 1 from server
        try:
            await asyncio.wait_for(asyncio.gather(
                asyncio.sleep(heartbeat_interval * jitter / 1000),
                interrupt_event.wait()
            ), timeout=(heartbeat_interval * jitter / 1000))
        except asyncio.TimeoutError:
            pass
        
        if interrupt_event.is_set():
            print("Interrupted: sending heartbeat immediately")
        '''

        await websocket.send(json.dumps(heartbeat_data))
        print("Heartbeat sent")
        jitter = 1

# 2: Identify
async def send_identify(shard):
    max_shards = websocket_connections["max_shards"]
    this_websocket_connection = websocket_connections["connections"][shard]
    websocket = this_websocket_connection["websocket"]

    print("Not identified yet, sending Identify...")
    identify_data = {
        "op": 2,
        "d": {
            "token": TOKEN,
            "intents": 4614, # (1 >> 1/2/9/12)
            "properties": {
                "os": "linux",
                "browser": "Hanabot", # change to "Discord Android" to appear as mobile
                "device": "Hanabot"
            },
            "presence": {
                "since": "null", 
                "activities": [{
                    "name": "Celeste",
                    "type": 0,
                    "state": "if u see this, ping hana (also ur cute <3)",
                    "since": 1420070400000
                }],
                "status": "online",
                "afk": False,
            },
            "shard": [shard, max_shards]
        }
    }
    await websocket.send(json.dumps(identify_data))
    print("Identify sent")

# 3: Update Presence
async def send_update_presence(shard, status, activities=None):
    this_websocket_connection = websocket_connections["connections"][shard]
    websocket = this_websocket_connection["websocket"]

    if status == "idle": afk = True
    else: afk = False
    
    if activities == None:
        update_presence_data = {
            "op": 3,
            "d": {
                "since": "null",
                "status": status,
                "afk": afk
            }
        }
    else:
        update_presence_data = {
            "op": 3,
            "d": {
                "since": "null",
                "activities": activities,
                "status": status,
                "afk": afk
            }
        }
    await websocket.send(json.dumps(update_presence_data))
    print("Presence Update sent")

# 4: Voice State Update
async def send_voice_state_update(websocket):
    data = {
        "op": 4,
        "d": {
            "guild_id": "1124006564920037416",
            "channel_id": "1134370744894226442",
            "self_mute": True,
            "self_deaf": True
        }
    }
    await websocket.send(json.dumps(data))

# 6: Resume
async def send_resume(shard):
    this_websocket_connection = websocket_connections["connections"][shard]

    websocket = this_websocket_connection["websocket"]
    last_sequence_number = this_websocket_connection["last_sequence_number"]
    seq = this_websocket_connection["last_sequence_number"]
    session_id = this_websocket_connection["session_id"]

    print(f"last_sequence_number: {last_sequence_number}, seq: {seq}")
    print("Already identified, need to send Resume")
    resume_data = {
        "op": 6,
        "d": {
            "token": TOKEN,
            "session_id": session_id,
            "seq": seq
        }
    }
    #print(resume_data)
    await websocket.send(json.dumps(resume_data))

# 8: Request Guild Members, I don't remember if this is even functional honestly
async def send_request_guild_members(websocket):
    print("Requesting guild members")
    request_data = {
        "op": 8,
        "d": {
            "guild_id": "1124006564920037416",
            "query": "",
            "limit": 0
        }
    }
    await websocket.send(json.dumps(request_data))

# actual connection stuff

# configure a websocket
async def run_websocket(shard, gateway_info, mode):
    websocket_connections["max_shards"] = gateway_info["shards"]

    while True:
        if mode == "Identify":
            websocket_connections["connections"][shard] = {
                # initial state information
                "session_id": "",
                "heartbeat_interval": 0,
                "last_sequence_number": None,
                "seq": None,
                "to_send_resume": False,
                "gateway_info": gateway_info,
                "resume_gateway_url": "",
                "shard": shard
            }
        elif mode == "Resume":
            websocket_connections["connections"][shard].update({"to_send_resume": True})

        status = await start_websocket(mode, shard)
        mode = status[0]

# start it
async def start_websocket(mode, shard):
    this_websocket_connection = websocket_connections["connections"][shard]

    gateway_info = this_websocket_connection["gateway_info"]
    resume_gateway_url = this_websocket_connection["resume_gateway_url"]

    if resume_gateway_url == "":
        print("Not sending resume")
        url = f"{gateway_info['url']}/?v=9&encoding=json" # format URL again
    else:
        print("Sending resume")
        url = resume_gateway_url
    
    max_shards = gateway_info["shards"]

    print(f"Connecting to {url}")
    if url == None: # fallback URL
        url = "wss://gateway.discord.gg/?v=9&encoding=json"
    
    try:
        async with websockets.connect(url) as websocket:
            this_websocket_connection["websocket"] = websocket
            while True:
                try:
                    message = await websocket.recv()
                    status = await on_message(shard, message)

                    # if there's extra info needed
                    if status:
                        if status[0] == "Resume":
                            await websocket.close()
                            return status

                except websockets.exceptions.ConnectionClosed as e:
                    # Connection closed
                    print(f"WebSocket connection closed with code: {e.code}")
                    if e.code == 4004 or e.code == 4010 or e.code == 4011 or e.code == 4012 or e.code == 4013 or e.code == 4014:
                        print("Cannot reconnect, exiting bot...")
                        exit()
                    else:
                        to_send_resume = True
                        print(f"resume_gateway_url: {resume_gateway_url} will restart bot now")
                        await start_websocket("Resume", shard)
    except websockets.exceptions.ConnectionClosedError as e:
        # Handle initial connection error
        print(f"Error connecting to WebSocket: {e}")
    except websockets.exceptions.ConnectionClosedError as e:
        # Connection closed
        print(f"WebSocket connection closed: {e}")
        print(f"WebSocket connection closed with code: {e.code}")

