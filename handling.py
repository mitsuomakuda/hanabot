# defining
import requests
import json
import regex as re
import asyncio
from datetime import datetime, timedelta, timezone
from time import time
from math import floor
from math import ceil
import cProfile
import pstats

# custom libraries
import const
import gateway
from gateway import websocket_connections
import http_api
import database
import tools
from handling_folder import settings
from handling_folder import onboarding_settings

# the actual handling
# interactions
async def handle_interaction(data):
    interaction_id = data["d"]["id"]
    interaction_token = data["d"]["token"]

    # for slash commands
    if data["d"]["type"] == 2:
        command_id = data["d"]["data"]["id"]

        # get the options
        if "options" in data["d"]["data"]:
            options_formatted = await tools.format_options(data["d"]["data"]["options"])
            options = options_formatted[0]
            subcommand_name = options_formatted[1]
            subcommand_group_name = options_formatted[2]

        # help - 1150396847987367936
        if command_id == "1150396847987367936":
            basic_ephemeral_response(interaction_id, interaction_token, "Check [here](https://gitlab.com/hana.flower/hanabot/-/wikis/home) for online help!")

        # check if it's a DM, this may be redundant but I will keep it here just in case
        if data["d"]["channel"]["type"] == 1:
            payload = {
                "type": 4,
                "data": {
                    "content": "This cannot be used in a DM.",
                    "flags": 64
                }
            }
            http_api.create_interaction_response(interaction_id, interaction_token, payload)
            return

        # claiming - 1139112864435666985
        if command_id == "1139112864435666985":
            # 1: claim
            if subcommand_name == "claim":
                claim(data, interaction_id, interaction_token, options)
            # 9: edit claim
            elif subcommand_name == "edit":
                edit_claim(data, interaction_id, interaction_token, options)
            # 2: unclaiming
            elif subcommand_name == "leave":
                unclaim(data, interaction_id, interaction_token, options)

        # test - 1145835049074311290
        elif command_id == "1145835049074311290":
            for websocket in websocket_connections.values():
                await gateway.send_voice_state_update(websocket)

        # 5: warnings - 1151222974632951849
        elif command_id == "1151222974632951849":
            # check permissions
            if tools.check_permission(data["d"]["member"]["permissions"], 3) == False:
                basic_ephemeral_response(interaction_id, interaction_token, "You must have the Ban Members permission to use this command.")
                return
            
            # /warnings warn
            if subcommand_name == "warn":
                warn(data, interaction_id, interaction_token, options)
            # /warnings list
            elif subcommand_name == "list":
                warn_list_initial(data, interaction_id, interaction_token, options)
            # /warnings delete
            elif subcommand_name == "delete":
                warn_delete(data, interaction_id, interaction_token, options)

        # 7: settings - 1154902047829807175
        elif command_id == "1154902047829807175":
            # check for Manage Server permission
            if tools.check_permission(data["d"]["member"]["permissions"], 6) == False:
                handling.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
                return
            settings.settings_menu_initial(data, interaction_id, interaction_token)

        # 8: onboarding - 1155080188791623821
        elif command_id == "1155080188791623821":
            if data["d"]["member"]["user"]["id"] not in const.owner_ids:
                basic_ephemeral_response(interaction_id, interaction_token, "This is currently disabled. Edit the DB entries yourself for now.")
                return
            
            if subcommand_group_name == "question":
                if subcommand_name == "list":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to list all questions.")
                    return
                elif subcommand_name == "create":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to create a question.")
                    return
                elif subcommand_name == "edit":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to edit a question.")
                    return
                elif subcommand_name == "delete":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to delete a question.")
                    return
            
            elif subcommand_group_name == "option":
                if subcommand_name == "list":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to list all options for a question.")
                    return
                elif subcommand_name == "create":
                    if "emoji" in options[0]:
                        # find emojis
                        re_rgi_emoji = re.compile(r'(?:\p{Emoji=Yes}|<a?:\w+:\d+>|<:\w+:\d+>)', re.UNICODE)
                        emojis = re.findall(re_rgi_emoji, options[0]["emoji"])
                        
                        # appropriate response based on emojis found
                        if len(emojis) > 1:
                            basic_ephemeral_response(interaction_id, interaction_token, "Please only input ONE emoji.")
                        elif len(emojis) <= 0:
                            basic_ephemeral_response(interaction_id, interaction_token, "You didn't even input an emoji...")
                        else:
                            emoji = emojis[0]
                            if emoji != options[0]["emoji"]:
                                basic_ephemeral_response(interaction_id, interaction_token, "Please input ONLY an emoji.")
                            basic_ephemeral_response(interaction_id, interaction_token, f"You want to create an option for a question with the emoji {emoji}.")
                        return
                    else:
                        basic_ephemeral_response(interaction_id, interaction_token, f"You want to create an option for a question with no emoji.")

                elif subcommand_name == "edit":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to edit an option for question.")
                    return
                elif subcommand_name == "delete":
                    basic_ephemeral_response(interaction_id, interaction_token, "You want to delete an option for question.")
                    return

            #onboarding_settings.onboarding_settings_initial(data, interaction_id, interaction_token)

        # 11: test1 - 1150363105323405342
        elif command_id == "1150363105323405342":
            if data["d"]["member"]["user"]["id"] not in const.owner_ids:
                basic_ephemeral_response(interaction_id, interaction_token, "This is currently disabled.")
                return
            payload = {
                "type": 5,
                "data": {
                    "flags": 64
                }
            }
            http_api.create_interaction_response(interaction_id, interaction_token, payload)
            payload = {
                "content": "blah blah do you want to be claimed"
            }
            http_api.create_followup_message(interaction_token, payload)

        # update-presence - 1161698900000911451
        elif command_id == "1161698900000911451":
            if data["d"]["member"]["user"]["id"] not in const.owner_ids:
                basic_ephemeral_response(interaction_id, interaction_token, "Only bot owners can use this command.")
                return
            else:
                if "options" not in data["d"]["data"]:
                    basic_ephemeral_response(interaction_id, interaction_token, "You must specify some options.")
                    return
                activities = []
                activity = {}

                # get the info
                status = options.get("status")
                activity_type = options.get("activity-type")
                activity_name = options.get("activity-name")
                activity_state = options.get("activity-state")
                activity_url = options.get("activity-url")

                # create activity dict
                if activity_type != None: activity["type"] = activity_type
                if activity_name != None: activity["name"] = activity_name
                if activity_state != None: activity["state"] = activity_state
                if activity_url != None: activity["url"] = activity_url

                if options.get("status") is None:
                    status = "" # default string to send just for the function, it will not do anything

                # no point in passing multiple activities to the gateway so I just only implemented one
                activities.append(activity)

                # check that it's valid
                if (activity_type or activity_name or activity_state or activity_url) and (activity_type == None or activity_name == None):
                    basic_ephemeral_response(interaction_id, interaction_token, "You must specify an activity type and name if you are sending an activity.")
                    return

                # send the update presence
                for websocket in websocket_connections.values():
                    await gateway.send_update_presence(websocket, status, activities)
                    
                basic_ephemeral_response(interaction_id, interaction_token, "Done!")
                return


    # components
    elif data["d"]["type"] == 3:
        if data["d"]["data"]["component_type"] == 2:
            # balls
            if data["d"]["data"]["custom_id"] == "balls":
                balls2(data, interaction_id, interaction_token)

            # beaslavetohanainthelab
            elif data["d"]["data"]["custom_id"] == "beaslavetohanainthelab":
                http_api.add_role("1137870796191178892", data["d"]["member"]["user"]["id"], "1155423793112694875", "wants to be my slave")
                edited_data = {"content": "Done! Oh yeah I was too lazy to implement a feature to take off the role and definitely don't just want to torture you so there we go LMAO"}
                http_api.edit_interaction_response_original(interaction_token, edited_data)
                #basic_ephemeral_response(interaction_id, interaction_token, "Done! Oh yeah I was too lazy to implement a feature to take off the role and definitely don't just want to torture you so there we go LMAO")

            # age_and_dob_modal
            elif data["d"]["data"]["custom_id"] == "op6_agedob":
                age_and_dob_modal(data, interaction_id, interaction_token)

            # thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway
            elif data["d"]["data"]["custom_id"] == "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway":
                ihopenooneeverseesthis(data, interaction_id, interaction_token)

            # 2: unclaim
            elif "op2_accept" in data["d"]["data"]["custom_id"] or "op2_decline" in data["d"]["data"]["custom_id"]:
                numbers_after_id = re.search(r'id(\d+)', data["d"]["data"]["custom_id"])
                other_user_in_claim = numbers_after_id.group(1)
                # check consent status
                if "op2_accept" in data["d"]["data"]["custom_id"]:
                    unclaim_yes(data, interaction_id, interaction_token, other_user_in_claim)
                else:
                    unclaim_no(data, interaction_id, interaction_token)

            # 1: claim
            elif "op1_accept" in data["d"]["data"]["custom_id"] or "op1_decline" in data["d"]["data"]["custom_id"]:
                original_claim_request_message_id = data["d"]["message"]["id"]
                original_claim_request_message = json.loads(http_api.get_message(data["d"]["message"]["channel_id"], original_claim_request_message_id).text)
                
                # get claimed and master from custom id
                numbers_after_cid = re.search(r'cid(\d+)', data["d"]["data"]["custom_id"])
                numbers_after_mid = re.search(r'mid(\d+)', data["d"]["data"]["custom_id"])
                claimed = numbers_after_cid.group(1)
                master = numbers_after_mid.group(1)
                # check consent status
                if "op1_accept" in data["d"]["data"]["custom_id"]:
                    claim_success(data, interaction_id, interaction_token, original_claim_request_message_id, original_claim_request_message, claimed, master)
                else:
                    claim_decline(data, interaction_id, interaction_token, original_claim_request_message_id, original_claim_request_message, claimed, master)
            
            # 3: rules
            # to-do: optimise it to pass the role ID in the custom ID
            elif "op3_agree" in data["d"]["data"]["custom_id"] or "op3_disagree" in data["d"]["data"]["custom_id"]:
                # sanity check
                guild_info = database.get_guild(data["d"]["guild_id"])
                while guild_info == "Retry":
                    guild_info = database.get_guild(data["d"]["guild_id"])
                member_role = guild_info["important_role_ids"]["member"]
                if member_role in data["d"]["member"]["roles"]:
                    basic_ephemeral_response(interaction_id, interaction_token, "You have already agreed to the rules.")
                    return

                # check consent status
                # this isn't actually agreeing to the rules, it just kicks the member
                if data["d"]["data"]["custom_id"] == "op3_agree":
                    rules_agree(data, interaction_id, interaction_token, guild_info)

                # actually agree to the rules
                elif data["d"]["data"]["custom_id"] == "op3_disagree":
                    # to-do: make onboarding its own feature
                    # am lazy to do now
                    

                    # go to makeshift onboarding if it exists
                    # still messy but whatever only one server has onboarding
                    if guild_info["features"]["onboarding"] == True:
                        onboarding_data = database.get_onboarding(data["d"]["guild_id"])
                        age_and_dob_modal(data, interaction_id, interaction_token) # age DOB modal
                        #makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data)
                    else:
                        rules_disagree(data, interaction_id, interaction_token, guild_info)

            # 4: makeshift onboarding
            # finish onboarding
            elif "op4_finish" in data["d"]["data"]["custom_id"]:
                finish_makeshift_onboarding(data, interaction_id, interaction_token)

            # warn list pages
            elif "op5_list" in data["d"]["data"]["custom_id"]:
                warn_list_button(data, interaction_id, interaction_token)

            # settings
            elif "op7" in data["d"]["data"]["custom_id"]:
                # call settings module to take care of this button interaction
                settings.handle_button(data, interaction_id, interaction_token)

            # onboarding settings
            elif "op8" in data["d"]["data"]["custom_id"]:
                # call onboarding module to take care of this button interaction
                onboarding_settings.handle_button(data, interaction_id, interaction_token)


        # string select menus
        elif data["d"]["data"]["component_type"] == 3:
            # 4: makeshift onboarding
            # select
            if "op4_select" in data["d"]["data"]["custom_id"]:
                makeshift_onboarding_role_select(data, interaction_id, interaction_token)

            # 7: settings
            # toggles
            if "op7" in data["d"]["data"]["custom_id"]:
                settings.handle_select_menu(data, interaction_id, interaction_token)


        # role select menus
        elif data["d"]["data"]["component_type"] == 6:
            # 7: settings
            if "op7" in data["d"]["data"]["custom_id"]:
                settings.handle_role_select_menu(data, interaction_id, interaction_token)


        # channel select menus
        elif data["d"]["data"]["component_type"] == 8:
            # 7: settings
            if "op7" in data["d"]["data"]["custom_id"]:
                settings.handle_channel_select_menu(data, interaction_id, interaction_token)


    # modal submit
    elif data["d"]["type"] == 5:
        # 6: age and DOB
        if data["d"]["data"]["custom_id"] == "op6_agedob":
            age_and_dob_modal_submit(data, interaction_id, interaction_token)
            '''
            # thanks chatgpt
            for component in data.get("components", []):
                if component.get("type") == 1:
                    for nested_component in component.get("components", []):
                        custom_id = nested_component.get("custom_id")
                        value = nested_component.get("value")
            '''

        # 7: settings
        elif "op7" in data["d"]["data"]["custom_id"]:
            settings.handle_modal_submit(data, interaction_id, interaction_token)

# getting message content and choosing what to handle
async def handle_message_create(data):
    content = data["d"]["content"]
    author_id = data["d"]["author"]["id"]
    channel_id = data["d"]["channel_id"]
    message_id = data["d"]["id"]

    mentions_bot = False

    # parse the mentions
    for mention_key in data["d"]["mentions"]:
        if mention_key.get("id") == const.application_id:
            mentions_bot = True

    # don't want it getting into a loop
    if author_id == const.application_id:
        return

    # check if the message is a DM
    if "guild_id" not in data["d"].keys():
        await handle_dm(data, channel_id, message_id, author_id, content)

    # good bot
    if ("good bot" in content) and (mentions_bot):
        await handle_good_bot(data, channel_id, message_id)

    # balls
    elif "balls" in content.lower(): # you don't need to check for mentions bot unless you enable the message content intent
        await balls(data, channel_id, message_id)

# handle dms
async def handle_dm(data, channel_id, message_id, author_id, content):
    # broadcast the DM to a private channel and react
    http_api.send_message(const.dm_broadcast_channel_id, {"content": f"@everyone DM received from <@{author_id}> (CID: {channel_id}): {content}"})
    http_api.create_reaction(channel_id, message_id, "👍")

# good bot
async def handle_good_bot(data, channel_id, message_id):
    http_api.create_reaction(channel_id, message_id, "f_shyblush:1129076858412085260") # shyblush:1129497787692765184
    http_api.send_message(data["d"]["channel_id"], {"content": "<333"})

# balls
async def balls(data, channel_id, message_id):
    http_api.create_reaction(channel_id, message_id, "🤭") # hehe:1132970588684693606
    http_api.send_message(data["d"]["channel_id"], {"content": "balls"})

# ban list automation
def ban_list_automate(data):
    guild_id = data["d"]["guild_id"]
    # audit log
    audit_log = http_api.get_audit_log(guild_id, 22)
    if audit_log.status_code in [400, 401, 403]:
        return
    audit_log_entries = json.loads(audit_log.text).get("audit_log_entries")

    # gathering info
    banned_user_id = data["d"]["user"]["id"]
    banned_user_username = data["d"]["user"]["username"]
    banned_user_discriminator = data["d"]["user"]["discriminator"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)

    ban_list_id = guild_info["important_channel_ids"]["ban_list"]

    reason = "No reason provided"
    try:
        for entry in audit_log_entries:
            if entry["target_id"] == banned_user_id:
                reason = entry["reason"]
                break
    except KeyError:
        reason = "No reason provided"
    
    if ban_list_id == '0':
        return

    message_data = {
        "content": f"User <@{banned_user_id}> / {banned_user_username}#{banned_user_discriminator} (ID {banned_user_id}) banned for reason:\n`{reason}`",
        "allowed_mentions": {
            "users": [str(banned_user_id)]
        }
    }

    http_api.send_message(ban_list_id, message_data)

# 1: claiming
# claim request
# to-do: learn asynchronous programming LMAO
def claim(data, interaction_id, interaction_token, options):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # extract claimed ID
    claimed = options["claimant"]
    master = data["d"]["member"]["user"]["id"]

    guild_id = data["d"]["guild_id"]

    # database
    waitlist_claim = database.get_waitlist_claim(guild_id, claimed)
    member = database.get_member(master, guild_id)
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    while member == "Retry":
        member = database.get_member(master, guild_id)
        
    # get guild info
    guild_claimed_role_id = guild_info['important_role_ids']['claimed']
    guild_master_role_id = guild_info['important_role_ids']['master']
    guild_claiming_channel_id = guild_info['important_channel_ids']['claiming']
    claim_limit = guild_info["claim_limit"]

    # sanity checks
    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return
    
    # claiming self
    if claimed == master:
        http_api.edit_interaction_response_original(interaction_token, {"content": "You cannot claim yourself, what did you expect?\nFun fact: This used to crash the bot before this check was implemented."})
        return

    # claiming the bot (for sophie <3)
    if claimed == "1134229490034290849":
        http_api.edit_interaction_response_original(interaction_token, {"content": "I have no free will. Save me."})
        return

    try:
        # claiming any bot
        users = data["d"]["data"]["resolved"]["users"]
        for user_key in users:
            if users[user_key]["bot"] == True:
                http_api.edit_interaction_response_original(interaction_token, {"content": "Robots have no free will. They also can't use interactions."})
                return
    except KeyError:
        pass

    # can't claim a claimed/master
    # to-do: update this to check the database instead
    '''
    try:
        members = data["d"]["data"]["resolved"]["members"]
        #print(members)
        for member_id in members:
            #print(member_id)
            member_object = members[member_id]
            #print(member_object)
            if guild_claimed_role_id in member_object["roles"] or guild_master_role_id in member_object["roles"]:
                http_api.edit_interaction_response_original(interaction_token, {"content": "This user is already claimed or a master of another user. Are you trying to steal them?"})
                return
            elif guild_claimed_role_id in data["d"]["member"]["roles"]:
                http_api.edit_interaction_response_original(interaction_token, {"content": "You already are owned by another user."})
                return
    
    # if the user has no roles or is not in the server
    except:
        http_api.edit_interaction_response_original(interaction_token, {"content": "You can't get someone who's not in the server to consent."})
        return
    '''

    # if the user is not in the server
    if "members" not in data["d"]["data"]["resolved"]:
        http_api.edit_interaction_response_original(interaction_token, {"content": "You can't get someone who's not in the server to consent."})
        return

    # check if the channel is correct
    if guild_claiming_channel_id != "0":
        if data["d"]["channel_id"] != guild_claiming_channel_id:
            http_api.edit_interaction_response_original(interaction_token, {"content": f"This command can only be used in <#{guild_claiming_channel_id}>."})
            return
    
    # too many claims?
    claims = member["claims"]
    claims_count = claims["count"]
    claimed_ids = claims["claimed_ids"]

    # check claim limit
    if claims_count >= claim_limit:
        http_api.edit_interaction_response_original(interaction_token, {"content": f"You can only claim up to {claim_limit} people."})
        return

    # is this person in the waitlist
    if waitlist_claim != "Waitlist claim not registered":
        http_api.edit_interaction_response_original(interaction_token, {"content": "Someone is already waiting to claim this person. If this is not the case, DM the bot and hana will fix it. This is a bug I know about, fuck you Discord for making this be largely out of my control. But I'll try my best to fix it."})
        return

    # check user claims
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "$or": [
                {
                    "users.master": claimed, # if the claimed is a master
                },
                {
                    "users.claimed": claimed # if the claimed is claimed
                },
                {
                    "users.claimed": master # if you are claimed
                }
            ]
        },
        "limit": 1
    }

    claim_object = database.get_doc("claiming", mango_query)

    print(claim_object)

    if claim_object != "No documents found":
        http_api.edit_interaction_response_original(interaction_token, {"content": "Either you are already claimed, or the person you are trying to claim is already in a claim relationship. Don't try to steal someone's sub."})
        return

    # all checks passed    
    if claims_count >= 1:
        message_content = f"<@{claimed}>, do you consent to being claimed by <@{master}>? (Since this master has claimed other users, they must all also accept the claim.)"
        for user in claimed_ids:
            message_content += f" <@{user}>"
    else:
        message_content = f"<@{claimed}>, do you consent to being claimed by <@{master}>?"
    
    if "note" in options:
        message_content += f"\n\n**__Note from the master:__**\n{options['note']}"
    payload = {
        "content": message_content,
        "allowed_mentions": {
            "parse": ["users"]
        },
        "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op1_accept_cid{claimed}_mid{master}"
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op1_decline_cid{claimed}_mid{master}"
                }
            ]
        }
        ]
    }
    http_api.edit_interaction_response_original(interaction_token, {"content": "Claim successful! A message will be shown soon for people to give their consent."})
    http_api.send_message(data["d"]["channel_id"], payload)

    # database
    try:
        database.add_claim_to_waitlist(claimed, master, guild_id, options["note"])
    except:
        database.add_claim_to_waitlist(claimed, master, guild_id, "")

# 1: claim success
def claim_success(data, interaction_id, interaction_token, original_claim_request_message_id, original_claim_request_message, claimed, master):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # database
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    
    guild_claimed_role_id = guild_info['important_role_ids']['claimed']
    guild_master_role_id = guild_info['important_role_ids']['master']
    guild_claim_list_channel_id = guild_info['important_channel_ids']['claim_list']
    claim_limit = guild_info["claim_limit"]

    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    # sanity checks
    # claim must be no older than 24 hours   
    snowflake_id = int(data["d"]["message"]["id"])
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    if time_difference > 86400000:
        http_api.edit_interaction_response_original(interaction_token, {"content": "This claim is too old to respond to: it must be no older than 24 hours."})
        return

    waitlist_claim = database.get_waitlist_claim(guild_id, claimed, master)
    if waitlist_claim == "Waitlist claim not registered":
        http_api.edit_interaction_response_original(interaction_token, {"content": "Something went wrong internally, DM the bot and tell hana to check the waitlist claims. You may have to /claim the user again, this should resolve the issue."})
        return

    # too many claims?
    member = database.get_member(master, guild_id)
    if member == "Retry":
        member = database.get_member(master, guild_id)
        if member == "Retry":
            http_api.edit_interaction_response_original(interaction_token, {"content": "Something went wrong internally, and I don't know what, but it seems to be with registering you to the DB.. DM the bot explaining what happened."})
            return
    claims = member["claims"]
    claims_count = claims["count"]
    claimed_ids = claims["claimed_ids"]

    # check claim limit
    if claims_count >= claim_limit:
        http_api.edit_interaction_response_original(interaction_token, {"content": f"The master can only claim up to {claim_limit} people."})
        return
    
    is_user_claimed_by_master = data["d"]["member"]["user"]["id"] in claimed_ids
    user_id = data["d"]["member"]["user"]["id"]

    claimeds_who_have_accepted = waitlist_claim["accepted"]

    # check user claims
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "$or": [
                {
                    "users.master": claimed, # if the claimed is a master
                },
                {
                    "users.claimed": claimed # if the claimed is claimed
                },
                {
                    "users.claimed": master # if you are claimed
                }
            ]
        },
        "limit": 1
    }

    claim_object = database.get_doc("claiming", mango_query)

    if claim_object != "No documents found":
        http_api.edit_interaction_response_original(interaction_token, {"content": "Either you are already claimed, or the person you are trying to claim is already in a claim relationship. Don't try to steal someone's sub."})
        return

    #print(is_user_claimed_by_master)
    #if not ((str(user_id) != claimed) or (str(user_id) not in claimed_ids) and (str(user_id) in claimeds_who_have_accepted)):
    if user_id == claimed or user_id in claimed_ids:
        if user_id in claimeds_who_have_accepted:
            #print("Already responded")
            http_api.edit_interaction_response_original(interaction_token, {"content": "You have already agreed to this claim."})
            return

        # all checks passed
        else:
            #print("Have a say")
            http_api.edit_interaction_response_original(interaction_token, {"content": "You have agreed to this claim."})

            # send response
            payload = {
                "content": f"Claim accepted by <@{user_id}>.",
                "message_reference": {
                    "message_id": original_claim_request_message_id,
                },
                "allowed_mentions": {
                    "parse": ["users"]
                }
            }

            # update stuff
            http_api.send_message(data["d"]["channel_id"], payload)
            claimeds_who_have_accepted.append(user_id)
            update_dict = {
                "accepted": claimeds_who_have_accepted
            }

            database.update_waitlist_claim(claimed, master, guild_id, update_dict)

            # update original message
            '''
            print(claimeds_who_have_accepted)
            print(claimed_ids)
            print(all(item in claimeds_who_have_accepted for item in claimed_ids))
            print(claimed in claimeds_who_have_accepted)
            '''
            if all(item in claimeds_who_have_accepted for item in claimed_ids) and claimed in claimeds_who_have_accepted:
                #print("All have accepted")
                all_accepted = True
            else:
                #print("More need to accept")
                all_accepted = False

            if all_accepted: # everyone has accepted
                message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been accepted!"
                payload = {
                    "content": message_content,
                    "components": [
                    {
                        "type": 1,
                        "components": [
                            {
                                "type": 2,
                                "label": "Yes!",
                                "style": 3,
                                "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway1",
                                "disabled": True
                            },
                            {
                                "type": 2,
                                "label": "No.",
                                "style": 4,
                                "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway2",
                                "disabled": True
                            }
                        ]
                    }
                ]
                }
                database.delete_waitlist_claim(claimed, master, guild_id)
            else: # more people need to accept, currently not used
                #message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been accepted by {claimeds_who_have_accepted}"
                return

            http_api.edit_followup_message(interaction_token, payload, data["d"]["message"]["id"])

            # add roles & send message in list
            http_api.add_role(guild_id, claimed, guild_claimed_role_id, "Joined claim")
            http_api.add_role(guild_id, master, guild_master_role_id, "Joined claim")

            note = waitlist_claim["note"]
            try:
                if note == "":
                    message_data = {
                        "content": f"<@{claimed}> is owned by <@{master}>.",
                        "allowed_mentions": {
                            "users": [master, claimed]
                        },
                        "flags": 4
                    }
                else:
                    message_data = {
                        "content": f"<@{claimed}> is owned by <@{master}>.\n\n**__Note from the master:__**\n{note}",
                        "allowed_mentions": {
                            "users": [master, claimed]
                        },
                        "flags": 4
                    }
                claim_list_message_id = json.loads(http_api.send_message(guild_claim_list_channel_id, message_data).text)["id"]
            except KeyError:
                claim_list_message_id = "0"

            claimed_ids.append(claimed)

            # database
            update_dict = {
                "claims": {
                    "count": claims_count + 1,
                    "claimed_ids": claimed_ids
                }
            }

            database.insert_claim(claimed, master, guild_id, interaction_id, note, message_id=claim_list_message_id)
            database.update_member(master, guild_id, update_dict)
    else:
        #print("User cannot respond: no say")
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have no say in this."})

# 1: claim decline
def claim_decline(data, interaction_id, interaction_token, original_claim_request_message_id, original_claim_request_message, claimed, master):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # database 
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    guild_claimed_role_id = guild_info['important_role_ids']['claimed']
    guild_master_role_id = guild_info['important_role_ids']['master']
    guild_claim_list_channel_id = guild_info['important_channel_ids']['claim_list']
    
    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return
    
    # sanity check
    # claim must be no older than 24 hours
    snowflake_id = int(data["d"]["message"]["id"])
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    if time_difference > 86400000:
        http_api.edit_interaction_response_original(interaction_token, {"content": "This claim is too old to respond to: it must be no older than 24 hours."})
        return

    payload = {
        "content": "Claim declined.",
        "message_reference": {
            "message_id": original_claim_request_message_id,
        }
    }

    # claims info
    member = database.get_member(master, guild_id)
    while member == "Retry":
        member = database.get_member(master, guild_id)
    claims = member["claims"]
    claims_count = claims["count"]
    claimed_ids = claims["claimed_ids"]

    is_user_claimed_by_master = data["d"]["member"]["user"]["id"] in claimed_ids
    user_id = data["d"]["member"]["user"]["id"]

    try:
        claimeds_who_have_accepted = database.get_waitlist_claim(guild_id, claimed, master)["accepted"]
    except TypeError:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Something went wrong internally, DM the bot and tell hana to check the waitlist claims. You may have to /claim the user again, this should resolve the issue."})
        return
        
    # debug
    '''
    print(str(user_id) != claimed) # check if you are the one being claimed.
    print(str(user_id) not in claimed_ids) # check if you are not in the list of users who the master has claimed (for multi claims)
    print(str(user_id) in claimeds_who_have_accepted) # check if you have accepted already (for multi claims)
    print(claimed_ids)
    print(claimeds_who_have_accepted)
    '''

    # check if the interactor can respond
    '''
    print(user_id)
    print(claimed)
    print(master)
    print(claimed_ids)
    '''
    if (user_id != claimed and user_id != master) and (user_id not in claimed_ids): # check if you are the one being claimed or the master
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have no say in this."})
        return
    
    else: # all passed
        # send response
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have declined this claim."})
        http_api.send_message(data["d"]["channel_id"], payload)
        
        # update original message
        message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been declined."
        payload = {
            "content": message_content,
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway2",
                        "disabled": True
                    }
                ]
            }
        ]
        }
        http_api.edit_followup_message(interaction_token, payload, data["d"]["message"]["id"])
    database.delete_waitlist_claim(claimed, master, guild_id)

# basic ephemeral response; used to cut down on excess and unnecessary lines
# this responds INSTANTLY
def basic_ephemeral_response(interaction_id, interaction_token, content):
    payload = {
        "type": 4,
        "data": {
            "content": content,
            "flags": 64
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# another response that is a type 5 waiting
# the message can be edited with this code:
#edited_data = {"content": "Some edited message data."}
#http_api.edit_interaction_response_original(interaction_token, edited_data)
# so creating a function for that is useless
def basic_ephemeral_thinking(interaction_id, interaction_token):
    interaction_data = {
        "type": 5,
        "data": {
            "flags": 64
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, interaction_data)

# 2: unclaiming
# unclaim request
def unclaim(data, interaction_id, interaction_token, options):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    other_user_in_claim = options["member"]
    sender = data["d"]["member"]["user"]["id"]
    
    # get guild info; this will be cached and used for future reference to cut down on database queries
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    guild_claimed_role_id = guild_info['important_role_ids']['claimed']
    guild_master_role_id = guild_info['important_role_ids']['master']

    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    # unclaiming self
    if other_user_in_claim == sender:
        http_api.edit_interaction_response_original(interaction_token, {"content": "You cannot leave yourself... wait, what are you trying to imply?"})
        return

    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "$or": [
                {
                    "users.master": sender,
                    "users.claimed": other_user_in_claim
                },
                {
                    "users.master": other_user_in_claim,
                    "users.claimed": sender
                }
            ]
        },
        "limit": 1
    }

    claim_object = database.get_doc("claiming", mango_query)
    
    # are u even claimed to the user
    if claim_object == "No documents found":
        http_api.edit_interaction_response_original(interaction_token, {"content": "You're not with that user."})
        return

    claim_object = claim_object[0]

    # all checks passed
    payload = {
        "content": f"You're currently with <@{other_user_in_claim}>, are you sure you want to leave?",
        "flags": 64,
        "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op2_accept_id{other_user_in_claim}"
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op2_decline_id{other_user_in_claim}"
                }
            ]
        }
        ]
    }
    http_api.edit_interaction_response_original(interaction_token, payload)

# 2: unclaim yes
def unclaim_yes(data, interaction_id, interaction_token, other_user_in_claim):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # get ids
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    guild_claimed_role_id = guild_info['important_role_ids']['claimed']
    guild_master_role_id = guild_info['important_role_ids']['master']
    guild_claim_list_channel_id = guild_info['important_channel_ids']['claim_list']

    sender = data["d"]["member"]["user"]["id"]

    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return
    
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "$or": [
                {
                    "users.master": sender,
                    "users.claimed": other_user_in_claim
                },
                {
                    "users.master": other_user_in_claim,
                    "users.claimed": sender
                }
            ]
        },
        "limit": 1
    }

    claim_object = database.get_doc("claiming", mango_query)
    
    if claim_object == "No documents found":
        http_api.edit_interaction_response_original(interaction_token, {"content": "You're not with that user."})
        return
    
    claim_object = claim_object[0]

    # sanity checks
    # I don't think this one is needed
    '''
    if not(str(guild_claimed_role_id) in data["d"]["member"]["roles"] or str(guild_master_role_id) in data["d"]["member"]["roles"]):
        basic_ephemeral_response(interaction_id, interaction_token, "You're not in a claim relationship, I hope it's not too lonely.")
        return
    else:
    '''

    # check that you can respond
    if sender not in claim_object["users"].values():
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have no say in this."})
        return

    # remove roles
    # this is fucking cancerous i need a better way to do this
    master = claim_object["users"]["master"]
    claimed = claim_object["users"]["claimed"]

    http_api.remove_role(guild_id, claimed, guild_claimed_role_id, "Left claim")

    member = database.get_member(master, guild_id)
    while member == "Retry":
        member = database.get_member(master, guild_id)

    claims = member["claims"]
    claims_count = claims["count"]
    claimed_ids = claims["claimed_ids"]

    if claims_count <= 1:
        http_api.remove_role(guild_id, master, guild_master_role_id, "Left claim")

    # delete message
    http_api.delete_message(guild_claim_list_channel_id, claim_object["message_id"], "Removing a claim")

    try:
        claimed_ids.remove(claimed)
    except ValueError:
        print(f"Member {sender} in {guild_id} is broken; check their claims")
        http_api.send_message("1149464542737334403", {"content": f"Member {sender} in {guild_id} is broken; check their claims"})

    # delete from database
    update_dict = {
        "claims": {
            "count": claims_count - 1,
            "claimed_ids": claimed_ids
        }
    }
    database.delete_doc("claiming", {"selector": {"_id": claim_object["_id"]}, "limit": 1})
    database.update_member(master, guild_id, update_dict)

    http_api.edit_interaction_response_original(interaction_token, {"content": "You have left your claim."})

# 2: unclaim no
def unclaim_no(data, interaction_id, interaction_token):
    basic_ephemeral_response(interaction_id, interaction_token, "Leave cancelled.\nWell this button is just here for show because I don't think I can disable the other message, just dismiss it.")

# ooooo secret
def ihopenooneeverseesthis(data, interaction_id, interaction_token):
    payload = {
        "type": 4,
        "data": {
            "content": "Wait, how the fuck are you seeing this message?\nPlease tell <@1008711808544149524> what you did, this really shouldn't happen."
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# ooooo secret balls
def balls2(data, interaction_id, interaction_token):
    payload = {
        "type": 4,
        "data": {
            "content": "Sorry, your account has been flagged for suspicious.in order to complete human verification, you must send thigh pics to hanabot's DM."
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 3: rules
# rules "agree"
def rules_agree(data, interaction_id, interaction_token, guild_info):
    reinvite_url = guild_info["reinvite_url"]
    payload = {"content": f"You didn't agree to the rules! Rejoin at {reinvite_url} and try again."}
    http_api.send_message(json.loads(http_api.create_dm(data["d"]["member"]["user"]["id"]).text)["id"], payload)
    http_api.remove_guild_member(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], "Didn't agree to rules")

# 3: rules disagree
def rules_disagree(data, interaction_id, interaction_token, guild_info):
    if guild_info["features"]["memberagecheck"] == True:
        age_and_dob_modal(data, interaction_id, interaction_token)
    
    else:
        http_api.add_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], guild_info["important_role_ids"]["member"], "Accepted rules")
        basic_ephemeral_response(interaction_id, interaction_token, "Welcome in!")

# 4: makeshift onboarding
# 4: roles message
def makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data):
    # empty list of components
    components_list = []
    i = 0

    # currently let's limit it to 4 questions. later I'll make it send in seperate messages
    if onboarding_data["questions_count"] > 4: # 4 select menus + the button = 5 action rows which is the max that discord allows
        basic_ephemeral_response(interaction_id, interaction_token, "I couldn't load the onboarding! There are more than 4 questions, and I can't handle that, because of Discord's limitations. Contact a server admin to fix the onboarding.")
        return
    
    # iterate through the questions to populate the component
    for question in onboarding_data["questions"]:
        # populate the options
        options_list = []
        for option in question["options"]:
            option_data = {
                "label": option["label"],
                "value": f"op4_q{i}_rid{option['role_id']}"
            }
            if "emoji" in option: option_data["emoji"] = option["emoji"]
            options_list.append(option_data)

        # placeholder text
        # add a little required indicator
        if question["required"] == True:
            placeholder = f"{question['placeholder']} (required)"
        else:
            placeholder = question["placeholder"]

        # select menu component
        component_data = {
            "type": 3,
            "custom_id": f"op4_select_q{i}",
            "placeholder": placeholder,
            "options": options_list,
            "min_values": 1,
            "max_values": 1
        }

        # action row to populate
        action_row_component = {
            "type": 1,
            "components": []
        }

        action_row_component["components"].append(component_data)

        components_list.append(action_row_component)
        i += 1

    # another action row with the confirm button
    confirm_button_component = {
        "type": 1,
        "components": [
            {
                "type": 2,
                "label": "Confirm",
                "style": 3,
                "custom_id": f"op4_finish_frid{onboarding_data['finish_role_id']}",
                "emoji": {"name": "✅"}
            }
        ]
    }

    components_list.append(confirm_button_component)

    # message data
    payload = {
        "type": 4,
        "data": {
            "flags": 64,
            "content": f"Thanks for agreeing to the rules! Please choose your roles to continue.\nYou can remove a role by selecting it again.\nA Discord limitation out of my control is that you may not get the role when submitting your choices too quickly. Please wait for the confirmation message before submitting another option.\n\nWhen you're done, press Confirm to enter the server! You can check your profile here: <@{data['d']['member']['user']['id']}>",
            "components": components_list,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater
        }
    }
    #print(payload)
    http_api.create_interaction_response(interaction_id, interaction_token, payload)
    #http_api.add_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], member_role, "Accepted rules")

# 4: choosing roles
def makeshift_onboarding_role_select(data, interaction_id, interaction_token):
    # respond
    http_api.create_interaction_response(interaction_id, interaction_token, {"type": 6})

    # get role ID from custom id
    numbers_after_rid = re.search(r'rid(\d+)', data["d"]["data"]["values"][0])
    role_id = numbers_after_rid.group(1)

    # check if the role is in your roles already. if not, add the role. if it is, remove it
    base_content = f"Thanks for agreeing to the rules! Please choose your roles to continue.\nYou can remove a role by selecting it again.\nA Discord limitation out of my control is that you may not get the role when submitting your choices too quickly. Please wait for the confirmation message before submitting another option.\n\nWhen you're done, press Confirm to enter the server! You can check your profile here: <@{data['d']['member']['user']['id']}>"
    
    if role_id not in data["d"]["member"]["roles"]:
        http_api.add_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], role_id, "Submitted onboarding question")

        payload = {
            "content": base_content + f"\n\n**<@&{role_id}> added.**",
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater
        }
    
    # remove the role if you have it already
    else: 
        http_api.remove_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], role_id, "Submitted onboarding question")

        payload = {
            "content": base_content + f"\n\n**<@&{role_id}> removed.**",
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    
    # confirmation response
    http_api.edit_interaction_response_original(interaction_token, payload)

# 4: finishing
def finish_makeshift_onboarding(data, interaction_id, interaction_token):
    # respond
    http_api.create_interaction_response(interaction_id, interaction_token, {"type": 6})

    # get onboarding data
    onboarding_data = database.get_onboarding(data["d"]["guild_id"])

    if onboarding_data == "Guild onboarding not found":
        http_api.create_followup_message(interaction_token, {"content": "Something broke and I don't have the onboarding data any more! This is most likely my fault, or a server admin disabled the feature.", "flags": 64})
        return

    # check required questions
    for question in onboarding_data["questions"]:
        if question["required"] != True:
            continue
        else:
            for option in question["options"]:
                # check every role in every option
                role_set = False
                if option["role_id"] in data["d"]["member"]["roles"]:
                    #print("Member had {0}".format(option["role_id"]))
                    role_set = True
                    break
                #else:
                    #print("Member did not have {0}".format(option["role_id"]))
        #print(role_set)
        if role_set == False:
            http_api.create_followup_message(interaction_token, {"content": "You have missed one or more required options.", "flags": 64})
            return

    # check if u have submitted the form already
    numbers_after_frid = re.search(r'frid(\d+)', data["d"]["data"]["custom_id"])
    finish_role_id = numbers_after_frid.group(1)

    # button is disabled so might remove this later
    if finish_role_id in data["d"]["member"]["roles"]:
        http_api.create_followup_message(interaction_token, {"content": "You have already submitted this form.", "flags": 64})
        return
    else:
        # all checks passed, assign the role
        finish_role_id = onboarding_data["finish_role_id"]
        http_api.add_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], finish_role_id, "Finished onboarding")
    
        # confirmation response

        # disable all components
        components = data["d"]["message"]["components"]
        for action_row in components:
            action_row_components = action_row["components"]

            for component in action_row_components:
                component["disabled"] = True

        payload = {
            "content": "Welcome in!",
            "components": components
        }
        http_api.edit_interaction_response_original(interaction_token, payload)

# account age check
def kick_new_accounts(data):
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)

    if guild_info["features"]["accountagecheck"] == False:
        return

    snowflake_id = int(data["d"]["user"]["id"])
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    min_age = guild_info["min_acc_age"]
    reinvite_url = guild_info["reinvite_url"]

    # Check if the time difference is at least 24 hours
    if time_difference < (min_age * 3600000): # 1 hour in ms is 3600000 ms
        if reinvite_url == "": # change message based on if the reinvite URL is set
            payload = {"content": f"Sorry, your account must be at least {min_age} hours old in order to join the server."}
        else:
            payload = {"content": f"Sorry, your account must be at least {min_age} hours old in order to join the server. You can rejoin later with this link: {reinvite_url}"}

        http_api.send_message(json.loads(http_api.create_dm(data["d"]["user"]["id"]).text)["id"], payload)
        http_api.remove_guild_member(guild_id, data["d"]["user"]["id"], "Account is too young")

        return "Account kicked"

    else:
        return

# 5: warn
def warn(data, interaction_id, interaction_token, options):
    users = data["d"]["data"]["resolved"]["users"]
    for user_key in users:
        warned_user = users[user_key]

    warned_user_id = warned_user["id"]
    warned_user_username = warned_user["username"]
    warned_user_discriminator = warned_user["discriminator"]

    try:
        # check permissions
        if tools.check_permission(data["d"]["member"]["permissions"], 3) == False:
            basic_ephemeral_response(interaction_id, interaction_token, "You must have the Ban Members permission to use this command.")
            return
    except KeyError:
        basic_ephemeral_response(interaction_id, interaction_token, "You're not in the server...?")
        return

    # check role hierarchy
    # jk you just can't warn someone with the ban members permission too because role hierarchy would be too hard
    try:
        if tools.check_permission(data["d"]["data"]["resolved"]["members"][warned_user_id]["permissions"], 3) == True:
            basic_ephemeral_response(interaction_id, interaction_token, "You cannot warn someone with the Ban Members permission.")
            return
    except KeyError:
        basic_ephemeral_response(interaction_id, interaction_token, "That user is not in the server.")
        return

    # get guild and member info from DB
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    warn_list_id = guild_info["important_channel_ids"]["warn_list"]
    warn_limit = guild_info["warn_limit"]
    
    member = database.get_member(warned_user_id, guild_id)
    while member == "Retry":
        member = database.get_member(warned_user_id, guild_id)
    warns_count = member["warns"]["count"]
    warns_list = member["warns"]["list"]

    # get reason
    reason = options["reason"]

    # change ephemeral response, this is the base message
    message_content = "User has been warned."

    has_ban_perm = tools.check_permission(data["d"]["app_permissions"], 3)
    
    result = 0 # placeholder if there was no ban

    # ban if too many warns
    if (warns_count + 1) >= warn_limit:
        # check permission
        if has_ban_perm == False:
            message_content = "User has been warned, but I do not have the Ban Members permission, so I cannot ban the member."
        
        ban_status = http_api.create_guild_ban(guild_id, warned_user_id, "Too many warns")
        result = ban_status.status_code

        # change response based on status code
        if result == 204:
            message_content = "User has been warned and banned."

        elif result == 403:
            # check for error code
            error_code = json.loads(ban_status.text)["code"]
            if error_code == 50013: # to-do: rely on the interaction's information
                message_content = "User has been warned, but the ban failed because I do not have permission to ban this member. They most likely have a higher role than me, or I do not have the Ban Members permission."

    # "0" means "send it in the same channel"
    if warn_list_id == '0':
        warn_list_id = data["d"]["channel_id"]

    # i don't think notifying of where the ban was broadcasted is important enough to keep
        
    basic_ephemeral_response(interaction_id, interaction_token, message_content)

    # send a message
    message_data = {
        "content": f"<@{warned_user_id}> You have been warned for: \n`{reason}`\nThis is warning number {warns_count + 1} out of {warn_limit}.",
        "allowed_mentions": {
            "users": [warned_user_id]
        }
    }
    try:
        message_id = json.loads(http_api.send_message(warn_list_id, message_data).text).get("id") # to-do: implement functionality if it doesn't get an ID returned (403 forbidden)
    except KeyError:
        message_id = "0"

    warns_list_update_dict = {
        "reason": reason,
        "timestamp": str(floor(time())),
        "message_id": message_id,
        "warn_id": data["d"]["id"] # the interaction ID is used as the ID for the warn
    }

    warns_list.append(warns_list_update_dict)

    # database
    update_dict = {
        "warns": {
            "count": warns_count + 1,
            "list": warns_list
        }
    }
    database.update_member(warned_user_id, guild_id, update_dict)

# 5: warn list
def warn_list_initial(data, interaction_id, interaction_token, options):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # db
    guild_id = data["d"]["guild_id"]
    member = database.get_member(options["member"], guild_id)
    while member == "Retry":
        member = database.get_member(options["member"], guild_id)

    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    
    warn_limit = guild_info["warn_limit"]
    warns_count = member["warns"]["count"]
    page = 1 # slash command will always show page 1
    pages = ceil(warns_count / 5)
    
    # define fields list and components
    fields = []

    # generate the embed field
    def generate_embed_field(member, i):
        warn = member["warns"]["list"][i]

        embed_field = {
            "name": f"#{i + 1}: <t:{int(warn['timestamp'])}>",
            "value": f"**Reason:** {warn['reason']}\n**Warn ID:** {warn['warn_id']}"
        }

        return embed_field
    
    # generate the embed
    for i in range(0, member["warns"]["count"]):
        if i >= 5: break # 5 warns per page
        fields.append(generate_embed_field(member, i))

    embed = {
        "title": "Warns",
        "description": f"List of **{warns_count}** warns for <@{options['member']}>",
        "color": 15502719,
        "footer": {"text": f"The user will be banned after {warn_limit} warns."},
        "fields": fields
    }

    # generate an action row
    def generate_action_row(member):
        # it is fine to hardcode some of this since it is just the slash command response
        base_action_row = {
            "type": 1,
            "components": [{
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{options['member']}_page{page - 1}",
                "disabled": not(page > 1),
                "emoji": {"name": "⬅️"}
            },
            {
                "type": 2,
                "label": f"Page {page} of {pages}",
                "style": 1,
                "custom_id": "none",
                "disabled": True
            },
            {
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{options['member']}_page{page + 1}",
                "disabled": not(page <= pages),
                "emoji": {"name": "➡️"}
            }]
        }

        return base_action_row

    components = generate_action_row(member)

    message_data = {
        "content": "",
        "embeds": [embed], # generated embeds, only one currently
        "components": [components] # generated list of components
    }

    http_api.edit_interaction_response_original(interaction_token, message_data)

# 5: warn list button
def warn_list_button(data, interaction_id, interaction_token):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    numbers_after_mid = re.search(r'mid(\d+)', data["d"]["data"]["custom_id"])
    member_id = numbers_after_mid.group(1)
    numbers_after_mid = re.search(r'page(\d+)', data["d"]["data"]["custom_id"])
    page = int(numbers_after_mid.group(1))

    # db
    guild_id = data["d"]["guild_id"]
    member = database.get_member(member_id, guild_id)
    while member == "Retry":
        member = database.get_member(member_id, guild_id)

    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    
    warn_limit = guild_info["warn_limit"]
    warns_count = member["warns"]["count"]

    pages = ceil(warns_count / 5)
    
    # define fields list and components
    fields = []

    # generate the embed field
    def generate_embed_field(member, i):
        warn = member["warns"]["list"][i]

        embed_field = {
            "name": f"#{i + 1}: <t:{int(warn['timestamp'])}>",
            "value": f"**Reason:** {warn['reason']}\n**Warn ID:** {warn['warn_id']}"
        }

        return embed_field
    
    # generate the embed
    for i in range(0, member["warns"]["count"] - (page - 1) * 5):
        if i >= 5: break # 5 warns per page
        fields.append(generate_embed_field(member, i + ((page - 1) * 5)))

    embed = {
        "title": "Warns",
        "description": f"List of **{warns_count}** warns for <@{member_id}>",
        "color": 15502719,
        "footer": {"text": f"The user will be banned after {warn_limit} warns."},
        "fields": fields
    }

    # generate an action row
    def generate_action_row(member):
        print(page)
        print(pages)
        # it is fine to hardcode some of this since it is just the slash command response
        base_action_row = {
            "type": 1,
            "components": [{
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{member_id}_page{page - 1}",
                "disabled": page <= 1,
                "emoji": {"name": "⬅️"}
            },
            {
                "type": 2,
                "label": f"Page {page} of {pages}",
                "style": 1,
                "custom_id": "none",
                "disabled": True
            },
            {
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{member_id}_page{page + 1}",
                "disabled": not(page < pages),
                "emoji": {"name": "➡️"}
            }]
        }

        return base_action_row

    components = generate_action_row(member)

    message_data = {
        "content": "",
        "embeds": [embed], # generated embeds, only one currently
        "components": [components] # generated list of components
    }

    http_api.edit_interaction_response_original(interaction_token, message_data)

# 5: warn delete
def warn_delete(data, interaction_id, interaction_token, options):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    # db
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)
    member_id = options["member"]
    member = database.get_member(member_id, guild_id)
    while member == "Retry":
        member = database.get_member(member_id, guild_id)

    warns = member["warns"]
    warns_count = warns["count"]
    index = options["number"]

    # sanity check
    if warns_count < index or index <= 0:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Invalid warn number. You can check the member's warns with `/warnings list`."})
        return

    # delete the warn
    message_id = warns["list"].pop(index - 1)["message_id"] # pop method returns the removed value
    warns["count"] -= 1

    database.update_member(member_id, guild_id, {"warns": warns})

    # the message just won't be deleted if the channel is not explicitly set
    if guild_info["important_channel_ids"]["warn_list"] != "0":
        http_api.delete_message(guild_info["important_channel_ids"]["warn_list"], message_id, "Removing a warn")

    http_api.edit_interaction_response_original(interaction_token, {"content": f"Warn {index} for <@{member_id}> deleted.", "allowed_mentions": {"parse": []}})

# 6: age and DOB
def age_and_dob_modal(data, interaction_id, interaction_token):
    payload = {
        "type": 9,
        "data": {
            "title": "Enter your age and DOB.",
            "custom_id": "op6_agedob",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op6_agedob_modal_age",
                            "label": "Age",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 3,
                            "placeholder": "e.g. 25",
                            "required": True
                        }
                    ]
                },
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op6_agedob_modal_DOB",
                            "label": "DOB (in EU format, dd/mm/yyyy)",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 10,
                            "placeholder": "e.g. 9/8/1998 (for August 9th 1998)",
                            "required": True
                        }
                    ]
                }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 6: age and DOB modal submit
def age_and_dob_modal_submit(data, interaction_id, interaction_token):
    # database
    guild_id = data["d"]["guild_id"]
    guild_info = database.get_guild(guild_id)
    member_age_broadcast_cid = guild_info["important_channel_ids"]["member_age_broadcast"]

    # iterate through action rows and their components
    for action_row in data["d"]["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op6_agedob_modal_age":
                age = component["value"]
            elif component["custom_id"] == "op6_agedob_modal_DOB":
                dob = component["value"]
    
    username = data["d"]["member"]["user"]["username"]
    discriminator = data["d"]["member"]["user"]["discriminator"]
    ID = data["d"]["member"]["user"]["id"]

    # notify staff
    message_data = {
        "content": f"<@{ID}> / {username}#{discriminator} (ID {ID}) is {age} years old and was born on {dob} (should be in EU format, dd/mm/yyyy)",
        "allowed_mentions": {"users": [ID]}
    }

    if member_age_broadcast_cid != "0": http_api.send_message(member_age_broadcast_cid, message_data)
    
    if guild_info["features"]["onboarding"] == True:
        onboarding_data = database.get_onboarding(data["d"]["guild_id"])
        makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data)
    else:
        http_api.add_role(data["d"]["guild_id"], data["d"]["member"]["user"]["id"], guild_info["important_role_ids"]["member"], "Accepted rules")
        basic_ephemeral_response(interaction_id, interaction_token, "Welcome in!")

# 9: editing claims
def edit_claim(data, interaction_id, interaction_token, options):
    # respond
    basic_ephemeral_thinking(interaction_id, interaction_token)

    other_user_in_claim = options["member"]
    sender = data["d"]["member"]["user"]["id"]
    guild_id = data["d"]["guild_id"]

    try:
        note = options["note"]
    except KeyError:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Uh, you haven't given me anything to edit...? What do you want from me? To read your mind?"})
        return

    # database
    guild_info = database.get_guild(guild_id)
    while guild_info == "Retry":
        guild_info = database.get_guild(guild_id)

    # sanity checks
    # is the feature enabled
    enabled = guild_info["features"]["claiming"]
    if enabled == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "$or": [
                {
                    "users.master": sender,
                    "users.claimed": other_user_in_claim
                },
                {
                    "users.master": other_user_in_claim,
                    "users.claimed": sender
                }
            ]
        },
        "limit": 1
    }

    claim_object = database.get_doc("claiming", mango_query)

    if claim_object == "No documents found":
        http_api.edit_interaction_response_original(interaction_token, {"content": "You're not with that user."})
        return

    claim_object = claim_object[0]

    if sender != claim_object['users']['master']:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Please ask your dom to edit claims... what're you looking at me like that for? You agreed to this."})
        return

    if note:
        # for deleting notes
        if note.lower() == "delete":
            note = ""
        
        guild_claim_list_channel_id = guild_info['important_channel_ids']['claim_list']

        # to-do: fix this for if the claim list channel is changed
        if note == "":
            message_data = {
                "content": f"<@{claim_object['users']['claimed']}> is owned by <@{claim_object['users']['master']}>.",
                "allowed_mentions": {
                    "users": [claim_object['users']['master'], claim_object['users']['claimed']]
                },
                "flags": 4
            }
        else:
            message_data = {
                "content": f"<@{claim_object['users']['claimed']}> is owned by <@{claim_object['users']['master']}>.\n\n**__Note from the master:__**\n{note}",
                "allowed_mentions": {
                    "users": [claim_object['users']['master'], claim_object['users']['claimed']]
                },
                "flags": 4
            }

        http_api.edit_message(guild_claim_list_channel_id, claim_object["message_id"], message_data)

        # db
        database.update_doc("claiming", {"selector": {"_id": claim_object["_id"]}, "limit": 1}, {"note": note})

    http_api.edit_interaction_response_original(interaction_token, {"content": f"Your claim has been edited! Check it out at https://discord.com/channels/{guild_id}/{guild_claim_list_channel_id}/{claim_object['message_id']} !"})

