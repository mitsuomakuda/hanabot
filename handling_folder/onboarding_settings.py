# custom libraries
# lmfao imagine dry run testing
import const
import gateway
import http_api
import database
import tools

# handle button
def handle_button(data, interaction_id, interaction_token):
    # check for Manage Server permission
    '''
    if tools.check_permission(data["d"]["member"]["permissions"], 6) == False:
        basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    '''

    custom_id = data["d"]["data"]["custom_id"]
    # create question
    if "op8_1createq" in custom_id:
        # base
        if custom_id == "op8_1createq":
            onboarding_settings_createq(data, interaction_id, interaction_token)

    # finish role
    if "op8_3finishrole" in custom_id:
        # base
        if custom_id == "op8_3finishrole":
            onboarding_settings_finishrole(data, interaction_id, interaction_token)


# 8: settings
def onboarding_settings_initial(data, interaction_id, interaction_token):
    # check for Manage Server permission
    '''
    if tools.check_permission(data["d"]["member"]["permissions"], 6) == False:
        basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    '''

    payload = {
        "type": 4,
        "data": {
            "content": "## Onboarding settings\n\nCurrently not functional.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Create new question",
                    "style": 1,
                    "custom_id": "op8_1createq"
                },
                {
                    "type": 2,
                    "label": "Delete a question",
                    "style": 1,
                    "custom_id": "op8_2deleteq"
                },
                {
                    "type": 2,
                    "label": "Set finish role",
                    "style": 1,
                    "custom_id": "op8_3finishrole"
                }]
            }]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 8.1 create question
def onboarding_settings_createq(data, interaction_id, interaction_token):
    payload = {
        "type": 4,
        "data": {
            "content": "## Onboarding settings  > Create question\n\nCreate an onboarding question.\n\nLet's start with the role! You may need to type the role's name if it doesn't appear in the dropdown. Currently still not functional.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op8_1createq_setrole",
                    "min_values": 0,
                }]
            }]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 8.3 set master role
def onboarding_settings_finishrole(data, interaction_id, interaction_token):
    payload = {
        "type": 4,
        "data": {
            "content": "## Onboarding settings  > Set finish role\n\nSet the role given to users when they finish onboarding, or no role if you do not want to give them one.\n\nYou may need to type the role's name if it doesn't appear in the dropdown. Currently still not functional.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op8_3finishrole_set",
                    "min_values": 0,
                }]
            }]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)