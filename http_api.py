import requests
import json
import const
import tools
from datetime import datetime, timedelta, timezone
from requests_toolbelt import MultipartEncoder
TOKEN = "Bot " + const.TOKEN
boundary = "boundary"

API_BASE_URL = "https://discord.com/api/v9"
basic_headers = {"Authorization": TOKEN, "User-Agent": f"DiscordBot Hanabot/v{const.git_commit_hash_short}"} # basic headers

# one universal function for sending and handling (when rate limiting is implemented) requests
def send_http_request(url, method, payload=None, custom_headers=None, multipart=False):
    headers = basic_headers
    if custom_headers: headers.update(custom_headers)

    # content type header, will be changed to multipart if it is needed
    if multipart == False:
        headers.update({"Content-Type": "application/json"})
    else:
        headers.update({"Content-Type": f"multipart/form-data; boundary={boundary}"})

    if not multipart and payload:
        payload = json.dumps(payload)

    # if there's a payload
    if payload:
        r = requests.request(method, url, headers=headers, data=payload)
    else:
        r = requests.request(method, url, headers=headers)

    print(r)
    print(r.text)

    # to-do later: implement rate limiting
    return r

# get gateway: 200
def get_gateway():
    r = requests.get(f"{API_BASE_URL}/gateway")
    url = json.loads(r.text)["url"]
    return url

# get gateway bot: 200
def get_gateway_bot():
    url = f"{API_BASE_URL}/gateway/bot"
    result = send_http_request(url, "GET")
    #print("Gateway and bot info returned by API:")
    #print(result.text)
    return result.text

# send a message: 200
def send_message(channel_id, message_data, attachment_data=False):
    url = f"{API_BASE_URL}/channels/{channel_id}/messages"

    if attachment_data:
        m = tools.encode_multipart(message_data)

        result = send_http_request(url, "POST", m, multipart=True)
        return result

    else:
        result = send_http_request(url, "POST", message_data)
        return result

# edit a message: 200
def edit_message(channel_id, message_id, message_data):
    url = f"{API_BASE_URL}/channels/{channel_id}/messages/{message_id}"

    result = send_http_request(url, "PATCH", message_data)
    return result

# create reaction: 204
def create_reaction(channel_id, message_id, emoji): # 204 on success
    emoji_encoded = tools.percent_encode(emoji)
    url = f"{API_BASE_URL}/channels/{channel_id}/messages/{message_id}/reactions/{emoji_encoded}/@me"

    result = send_http_request(url, "PUT")
    return result

# get audit log: 200
def get_audit_log(guild_id, action_type):
    url = f"{API_BASE_URL}/guilds/{guild_id}/audit-logs?action_type={action_type}"

    result = send_http_request(url, "GET")
    return result

# add role: 204
def add_role(guild_id, user_id, role_id, reason):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/guilds/{guild_id}/members/{user_id}/roles/{role_id}"

    result = send_http_request(url, "PUT", custom_headers=headers_audit_log)
    return result

# remove role: 204
def remove_role(guild_id, user_id, role_id, reason):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/guilds/{guild_id}/members/{user_id}/roles/{role_id}"

    result = send_http_request(url, "DELETE", custom_headers=headers_audit_log)
    return result

# get guild roles: 200
def get_guild_roles(guild_id):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/guilds/{guild_id}/roles"

    result = send_http_request(url, "GET")
    return result

# get specific message: 200
def get_message(channel_id, message_id):
    url = f"{API_BASE_URL}/channels/{channel_id}/messages/{message_id}"

    result = send_http_request(url, "GET")
    return result

# delete specific message: 204
def delete_message(channel_id, message_id, reason):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/channels/{channel_id}/messages/{message_id}"

    result = send_http_request(url, "DELETE", custom_headers=headers_audit_log)
    return result

# get guild member: 200
def get_guild_member(guild_id, user_id):
    url = f"{API_BASE_URL}/guilds/{guild_id}/members/{user_id}"

    result = send_http_request(url, "GET")
    return result

# remove guild member: 204
def remove_guild_member(guild_id, user_id, reason):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/guilds/{guild_id}/members/{user_id}"

    result = send_http_request(url, "DELETE", custom_headers=headers_audit_log)
    return result

# create guild ban: 204
def create_guild_ban(guild_id, user_id, reason, delete_message_seconds=0):
    headers_audit_log = {"X-Audit-Log-Reason": reason}
    url = f"{API_BASE_URL}/guilds/{guild_id}/bans/{user_id}"

    payload = {"delete_message_seconds": delete_message_seconds}

    result = send_http_request(url, "PUT", custom_headers=headers_audit_log, payload=payload)
    return result

# create DM: 200
def create_dm(user_id):
    payload = {"recipient_id": user_id}
    url = f"{API_BASE_URL}/users/@me/channels"

    result = send_http_request(url, "POST", payload=payload)
    return result

# start typing: 204
def start_typing(channel_id):
    url = f"{API_BASE_URL}/channels/{channel_id}/typing"

    result = send_http_request(url, "POST")
    return result

# INTERACTION STUFF

# interaction responses
# create response: 204
def create_interaction_response(interaction_id, interaction_token, message_data):
    url = f"{API_BASE_URL}/interactions/{interaction_id}/{interaction_token}/callback"

    result = send_http_request(url, "POST", payload=message_data)
    return result

# edit original response: 200
def edit_interaction_response_original(interaction_token, message_data):
    url = f"{API_BASE_URL}/webhooks/{const.application_id}/{interaction_token}/messages/@original"

    result = send_http_request(url, "PATCH", payload=message_data)
    return result

# get original response: 200
def get_interaction_response_original(interaction_token):
    url = f"{API_BASE_URL}/webhooks/{const.application_id}/{interaction_token}/messages/@original"

    result = send_http_request(url, "GET")
    return result

# followup messages
# create followup message: 200
def create_followup_message(interaction_token, message_data):
    url = f"{API_BASE_URL}/webhooks/{const.application_id}/{interaction_token}"

    result = send_http_request(url, "POST", payload=message_data)
    return result

# edit followup message: 200
def edit_followup_message(interaction_token, message_data, message_id):
    url = f"{API_BASE_URL}/webhooks/{const.application_id}/{interaction_token}/messages/{message_id}"

    result = send_http_request(url, "PATCH", payload=message_data)
    return result

