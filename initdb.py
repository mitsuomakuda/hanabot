# i don't think this is how you use couchdb but whatever
import couchdb
import os
import argparse
import json
import const
import update_db_docs
from time import time
from time import sleep
from math import floor
global version
version = None

# register cmds
# if they are already registered, they will not be changed if this file is run again
def registercmds(include_test_cmds):
    # very messy but it's ok
    commands = []

    claiming_cmd = {"name": "claiming",
        "description": "Assert your dominance over your pets!",
        "dm_permission": False,
        "options": [{
            "type": 1,
            "name": "claim",
            "description": "Claim someone like a little pet!",
            "options": [{
                "type": 6,
                "name": "claimant",
                "description": "Who do you want to claim? This person will be your sub.",
                "required": True
            },{
                "type": 3,
                "name": "note",
                "description": "Add extra info about your claim!",
                "max_length": 250
            }]
        },{
            "type": 1,
            "name": "edit",
            "description": "Modify info about one of your claims!",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "The person whose claim you want to modify info about",
                "required": True
            },{
                "type": 3,
                "name": "note",
                "description": "Add extra info about your claim! You can delete the note by inputting \"Delete\"",
                "max_length": 250
            }]
        },{
            "type": 1,
            "name": "leave",
            "description": "Want to leave your claim? That's fine!",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Who do you want to leave? If the user is not in the server, you must use their user ID",
                "required": True
            }]
        }],
    "nsfw": True}

    warnings_cmd = {"name": "warnings",
        "description": "Someone's being a naughty member~",
        "dm_permission": False,
        "options": [{
            "type": 1,
            "name": "warn",
            "description": "Warn someone.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Who do you want to warn?",
                "required": True
            },{
                "type": 3,
                "name": "reason",
                "description": "Why?",
                "required": True
            }]
        },{
            "type": 1,
            "name": "list",
            "description": "List someone's warns.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Whose warns you want to check.",
                "required": True
            }]
        },{
            "type": 1,
            "name": "delete",
            "description": "Delete a warn.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Whose warns you want to delete.",
                "required": True
            },{
                "type": 4,
                "name": "number",
                "description": "The warn number you want to delete, e.g. warn #1.",
                "required": True
            }]
        }],
    "nsfw": False}

    update_presence_cmd = {
        "name": "update-presence",
        "description": "Only bot owners can use this. Updates the bot's presence.",
        "dm_permission": True,
        "options": [{
            "type": 3,
            "name": "status",
            "description": "The status of the bot",
            "choices": [{
                    "name": "Online",
                    "value": "online"
                },{
                    "name": "Idle",
                    "value": "idle"
                },{
                    "name": "Do Not Disturb",
                    "value": "dnd"
                },{
                    "name": "Invisible (why?)",
                    "value": "invisible"
                }]
            },{
                "type": 4,
                "name": "activity-type",
                "description": "The activity type",
                "choices": [{
                        "name": "Playing {name}",
                        "value": 0
                    },{
                        "name": "Streaming {name} (url can be set to a twitch or youtube link and will appear on the bot's profile)",
                        "value": 1
                    },{
                        "name": "Listening to {name}",
                        "value": 2
                    },{
                        "name": "Watching {name}",
                        "value": 3
                    },{
                        "name": "Custom status: {emoji} {state} (emoji cannot be set)",
                        "value": 4
                    },{
                        "name": "Competing in {name}",
                        "value": 5
                    }],
                "min_value": 0,
                "max_value": 5
            },{
                "type": 3,
                "name": "activity-name",
                "description": "The activity name"
            },{
                "type": 3,
                "name": "activity-state",
                "description": "The activity state"
            },{
                "type": 3,
                "name": "activity-url",
                "description": "The activity URL"
            }],
    "nsfw": False}

    settings_cmd = {
        "name": "settings",
        "description": "Change my settings!",
        "dm_permission": False,
        "nsfw": False
    }

    help_cmd = {
        "name": "help",
        "description": "Lists available commands and help I can give you~",
        "dm_permission": True,
        "nsfw": False
    }

    # test commands
    if include_test_cmds == True:
        print("You are including experimental and broken test commands that are not at all maintained. God help you.")
        test_cmd = {
            "name": "test",
            "description": "test",
            "dm_permission": True,
            "nsfw": False
        }

        test1_cmd = {
            "name": "test1",
            "description": "test command",
            "dm_permission": True,
            "nsfw": False
        }

        onboarding_cmd = {"name": "onboarding",
            "description": "Let me welcome the new members!",
            "dm_permission": True,
            "options": [{
                "type": 2,
                "name": "question",
                "description": "Modify an onboarding question",
                "options": [{
                    "type": 1,
                    "name": "list",
                    "description": "List all onboarding questions."
                },{
                    "type": 1,
                    "name": "create",
                    "description": "Create an onboarding question."
                },{
                    "type": 1,
                    "name": "edit",
                    "description": "Edit an onboarding question."
                },{
                    "type": 1,
                    "name": "delete",
                    "description": "Delete an onboarding question."
                }]
            },{
                "type": 2,
                "name": "option",
                "description": "Modify the options for an onboarding question",
                "options": [{
                    "type": 1,
                    "name": "list",
                    "description": "List all options for an onboardng question."
                },{
                    "type": 1,
                    "name": "create",
                    "description": "Create an option for an onboarding question.",
                    "options": [{
                            "type": 3,
                            "name": "emoji",
                            "description": "The emoji you want to use",
                            "max_length": 200
                        }]
                },{
                    "type": 1,
                    "name": "edit",
                    "description": "Edit an option for an onboarding question."
                },{
                    "type": 1,
                    "name": "delete",
                    "description": "Delete an option for an onboarding question."
                }]}],
            "nsfw": False
        }

        commands.append(test_cmd)
        commands.append(test1_cmd)
        commands.append(onboarding_cmd)

    # append commands to list
    commands.append(claiming_cmd)
    commands.append(warnings_cmd)
    commands.append(update_presence_cmd)
    commands.append(settings_cmd)
    commands.append(help_cmd)

    # make the commands exist
    import requests
    url = f"https://discord.com/api/v10/applications/{const.application_id}/commands"
    headers = {"Authorization": const.TOKEN, "User-Agent": "DiscordBot CustomLibrary/v0.0", "Content-Type": "application/json"}
    command_ids = {}

    for command in commands:
        r = requests.post(url, headers=headers, data=json.dumps(command))
        print(r)
        print(r.text)
        if r.status_code == 401:
            print("Your bot token was invalid, please set up const.py first.")
            exit()
        command_ids[command["name"]] = json.loads(r.text)["id"]
        sleep(5)

    print("Command IDs:")
    print(command_ids)

# setup the database
def setupdb():
    # connect to the CouchDB server
    server = couchdb.Server(const.couchdb_server)
    server.resource.credentials = (const.couchdb_username, const.couchdb_password)

    try:
        claiming_db = server.create("claiming")  # Create a new database if it doesn't exist
    except couchdb.http.PreconditionFailed:
        claiming_db = server["claiming"]  # Database already exists

    try:
        guilds_db = server.create("guilds")
    except couchdb.http.PreconditionFailed:
        guilds_db = server["guilds"]

    try:
        members_db = server.create("members")
    except couchdb.http.PreconditionFailed:
        members_db = server["members"]

    try:
        claims_waitlist_db = server.create("claims_waitlist")
    except couchdb.http.PreconditionFailed:
        claims_waitlist_db = server["claims_waitlist"]

    try:
        makeshift_onboarding_db = server.create("makeshift_onboarding")
    except couchdb.http.PreconditionFailed:
        makeshift_onboarding_db = server["makeshift_onboarding"]

    if version == None:
        update_db_docs.update_entire_db("claiming")

# list all commands then exit
def list_command_ids():
    import requests
    url = f"https://discord.com/api/v10/applications/{const.application_id}/commands"
    headers = {"Authorization": const.TOKEN, "User-Agent": "DiscordBot CustomLibrary/v0.0", "Content-Type": "application/json"}
    r = requests.get(url, headers=headers)

    print(r)
    if r.status_code == 401:
        print("Your bot token was invalid, please set up const.py first.")
        exit()

    commands = json.loads(r.text)
    command_ids = {}

    for command in commands:
        command_ids[command["name"]] = command["id"]

    print(command_ids)
    exit()

# do stuff
if __name__ == "__main__":
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("--include-test-commands", action="store_true", help="Registers test commands too. You probably shouldn't use this because they are not maintained.")
    parser.add_argument("--list-command-ids", action="store_true", help="Lists your bot's command IDs, useful if you forgot or didn't save them.")
    args = parser.parse_args()

    if args.list_command_ids: list_command_ids()

    # lock on running initdb again
    lock_file = "initdb.lock"
    if os.path.exists(lock_file):
        with open(lock_file, "r") as file:
            version = file.readline()
            if version == "2": # current version of initdb
                print("Fatal: The database is already set up. There is no need to run this file again. But if you do want to run this again for any reason, you can delete the initdb.lock file.")
                exit()

    registercmds(args.include_test_commands)
    setupdb()

    # Create a file
    with open(lock_file, "w") as file:
        file.write("2")

    print("Done!")
    print("Please modify the command IDs in handling.py's handle_interaction() function. The IDs of every command were printed to the screen just now.")
