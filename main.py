# defining
import asyncio
import json
import time
from os import path

# custom libraries
import handling
import http_api
import database
import gateway
from gateway import websocket_connections
import const

print(f"Hanabot version {const.git_commit_hash_short}")
print("Home page: https://gitlab.com/hana.flower/hanabot")
print("Press CTRL + C to close the bot.\n")

# check database setup status
lock_file = "initdb.lock"
if path.exists(lock_file):
    with open(lock_file, "r") as file:
        content = file.readlines()[0]
        if content != "2": # current version of initdb
            print("Fatal: Your database is out of date. Please run the initdb.py file to update the database.")
            exit()
else:
    print("Fatal: Your database is not set up. Please run the initdb.py file in order to set up the CouchDB database.")
    exit()

# more defining
interrupt_event = asyncio.Event()
stop_heartbeat_event = asyncio.Event()

# gets the gateway info and URL
global GATEWAY_INFO
GATEWAY_INFO = json.loads(http_api.get_gateway_bot())
GATEWAY_URL = f"{GATEWAY_INFO['url']}/?v=9&encoding=json"

# maybe these globals are not necessary
global last_sequence_number
last_sequence_number = None
global seq
seq = None
global session_id
global resume_gateway_url
resume_gateway_url = ""
global resume_gateway_url_to_use
global to_send_resume
to_send_resume = False
global identified
identified = False

# I am too stupid to make it restart when your internet cuts out, sorry
async def main():
    # you can tell by the comments that chatgpt wrote this
    # I'll keep it in as a fun experiment for now, better than having no sharding
    # and it doesn't seem to mess with the bot's main function
    # in fact it seems to work just fine, but it's chatgpt-written so who really knows
    
    tasks = []  # Create a list to store the tasks

    for shard in range(0, GATEWAY_INFO["shards"]):
        print(f"Starting shard {shard}")
        task = asyncio.create_task(gateway.run_websocket(shard, GATEWAY_INFO, "Identify"))
        tasks.append(task)  # Append the task to the list

    # Wait for all tasks to complete
    await asyncio.gather(*tasks)

# for closing the bot
async def cleanup():
    print("Turning off bot... it may take up to a minute to appear offline")
    for shard in websocket_connections["connections"].keys():
        activities = [{
            "name": "Bot turning off, commands will not work.",
            "type": 0
        }]
        await gateway.send_update_presence(shard, "idle", activities)

# unleash a monster onto the world of Discord
if __name__ == "__main__":
    try:
        # Create a new asyncio event loop
        loop = asyncio.new_event_loop()

        # Set the newly created event loop as the current event loop
        asyncio.set_event_loop(loop)

        # Run the main function within the event loop
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        loop.run_until_complete(cleanup())
        pass