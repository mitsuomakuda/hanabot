import requests
import json
import const
TOKEN = "Bot " + const.TOKEN

headers = {'Authorization': TOKEN, 'Content-Type':'application/json', 'User-Agent': 'DiscordBot CustomLibrary/v0.0'}
url = f"https://discord.com/api/v10/applications/{const.application_id}/commands"
# add /<commandid> and change method in `requests.post` to patch instead of post to update a command
# change method to delete and remove the data parameter to delete a command

payload = {
    # command data here...
}

payload_json = json.dumps(payload)  # Convert payload dictionary to JSON string

r = requests.post(url, headers=headers, data=payload_json)

print(r)
print(r.text)
'''
output = json.dumps(json.loads(r.text), indent=4)

outFile = open('test.json', 'w')
outFile.write(str(output))
'''
