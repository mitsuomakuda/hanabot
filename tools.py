import requests
import json
import const
import urllib.parse
from requests_toolbelt import MultipartEncoder

# format message
def format_message(json_object, string_to_replace, what_to_replace_with):
    json_str = json.dumps(json_object)  # Convert the JSON object to a string
    replaced_json_str = json_str.replace(str(string_to_replace), str(what_to_replace_with))
    replaced_json_object = json.loads(replaced_json_str)  # Convert the modified string back to a dictionary
    return replaced_json_object

# percent encoding
def percent_encode(original_string):
    encoded_string = urllib.parse.quote(original_string)
    return encoded_string

# permissions integer to permissions
def check_permission(permissions_integer, bit_to_check):
    permissions_integer = int(permissions_integer)
    permissions_binary = bin(permissions_integer)[2:] # remove 0b prefix
    permissions_binary = permissions_binary.zfill(53) # the discord permission bitfield is 53 bits long, so fill the binary number to match that
    is_set = permissions_binary[-(bit_to_check)] == '1' # the bit will be 1 if it is set
    return is_set

# encode multipart
def encode_multipart(message_data):
    # json data
    fields = {
        'payload_json': (None, json.dumps(message_data), 'application/json')
    }

    # attachments
    for attachment in message_data["attachments"]:
        attachment_id = str(attachment["id"])
        filename = attachment["filename"]
        fields.update({f"files[{attachment_id}]": (filename, open(filename, "rb"), "image/png")}) # cry about it if it's not a png image xd

    m = MultipartEncoder(fields, boundary = "boundary")

    return m

# format options
async def format_options(options):
    # get the options
    print(f"Options are {options}")
    subcommand_name = None
    subcommand_group_name = None
    options_dict = {}
    # format the options into an easier format to work with
    for option in options:
        print(f"Checking an option {option}")
        if option["type"] == 1: # subcommand
            subcommand_name = option["name"]
            print(f"Subcommand name is {subcommand_name}")
            
            print("Checking subcommand options")
            subcommand_options = await format_options(option["options"])
            #print(subcommand_options)
            options_dict = subcommand_options[0]

        elif option["type"] == 2: # subcommand group
            subcommand_group_name = option["name"]
            print(f"Subcommand group name is {subcommand_group_name}")
            
            # call this function again to get the group options
            subcommand_group_options = await format_options(option["options"])
            options_dict = subcommand_group_options
            subcommand_name = option["options"][0]["name"]
            print(f"Subcommand name is {subcommand_name}")

        elif option["type"] in [3, 4, 6]: # string, integer or user
            options_dict[option["name"]] = option["value"]

        #print(f"Options dict {options_dict}")

    print(f"Final return value for options dict: {options_dict}")
    print(f"Final return value for subcommand name: {subcommand_name}")
    print(f"Final return value for subcommand group name: {subcommand_group_name}")
    return options_dict, subcommand_name, subcommand_group_name