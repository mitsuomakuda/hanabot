# credit to chatGPT lmao
import couchdb
import const
import sys

# connect to the CouchDB server
server = couchdb.Server(const.couchdb_server)
server.resource.credentials = (const.couchdb_username, const.couchdb_password)

# members
def update_members_version(doc):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    # now we repeat this for every update, which should give us an up to date member
    if "warns" not in doc:
        update_keys["warns"] = {"count": 0, "list": []}
        final_version = "2"
    
    return update_specific_doc_with_keys("members", final_version, update_keys, doc["_id"])

# guilds
def update_guilds_version(doc):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    # now we repeat this for every update, which should give us an up to date guild
    if "warn_list" not in doc["important_channel_ids"]:
        update_keys = {'important_channel_ids': {'warn_list': '0'}}
        final_version = "2"

    if "features" not in doc:
        # default, hardcoded values before this system
        update_keys["features"] = { 
            "claiming": True,
            "accountagecheck": True,
            "welcoming": True,
            "banlist": True
        }
        update_keys["min_acc_age"] = 24
        update_keys["reinvite_url"] = ""
        update_keys["warn_limit"] = 3
        final_version = "3"

    if "claim_limit" not in doc:
        update_keys["claim_limit"] = 3
        final_version = "4"

    if "member_age_broadcast" not in doc["important_channel_ids"]:
        update_keys = {'important_channel_ids': {'member_age_broadcast': '0'}}
        final_version = "5"

    if "memberagecheck" not in doc["features"]: # only need to check one thing this time
        update_keys = {"features": {"memberagecheck": False, "onboarding": False}}
        update_keys["rules_buttons_swapped"] = False
        update_keys["rules_message_link"] = ""
        final_version = "6"
    
    # insert everything into the document by couchDB ID
    return update_specific_doc_with_keys("guilds", final_version, update_keys, doc["_id"])

# claiming
# I hate how the db name is claiming and not claims but whatever it'll stick for now
def update_claiming_version(doc, ID="-1"):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    if "features" not in doc:
        # default, hardcoded values before this system
        update_keys["id"] = ID # the function may pass an ID to insert if not already done
        update_keys["users"] = {
            "master": doc.get("master_id"),
            "claimed": doc.get("claimed_id")
        }
        update_keys["note"] = ""
        final_version = "2"

    # insert everything into the document by couchDB ID
    return update_specific_doc_with_keys("claiming", final_version, update_keys, doc["_id"])

# update a specific document by couchhdb ID
def update_specific_doc_with_keys(doc_type, final_version, key_value_pairs, doc_id):
    db = server[doc_type]

    doc = db[doc_id]

    for key, value in key_value_pairs.items():
        if isinstance(value, dict):
            doc[key] = {**doc.get(key, {}), **value}
        else:
            doc[key] = value

    # update the version number
    doc["version"] = final_version

    db[doc_id] = doc

    return doc

# update an entire database
def update_entire_db(doc_type):
    db = server[doc_type]

    for doc_id in db:
        doc = db[doc_id]
        update_function = getattr(sys.modules[__name__], f'update_{doc_type}_version')
        updated_doc = update_function(doc)

#update_entire_db("claiming")

'''
# Example usage:
update_keys = {
    'important_channel_ids': {'warn_list': '0'},
}

update_docs_with_keys('guilds', 2, update_keys)
'''

# fucking chatgpt, this code does not work
'''
# update documents to a newer version
def update_docs_with_keys(doc_type, version, key_value_pairs):
    version = str(version)
    db = server[doc_type]

    for doc_id in db:
        doc = db[doc_id]

        if doc.get('version', 0) < version:
            important_channel_ids = doc.setdefault('important_channel_ids', {})
            important_channel_ids['warn_list'] = '0'

            # Update the "version" key
            doc['version'] = str(version)

            # Save the updated document back to the database
            db.save(doc)

            # Print a message indicating the update
            guild_id = doc.get('guild_id', 'Unknown')
            print(f"Updated document with guild_id {guild_id} to version {version}.")
'''